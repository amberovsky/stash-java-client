Overview
========
The Stash Java Client, is a small, light-weight Java library that can be used to interface with a Stash instance via its REST API.

The Stash Java Client library consists of the following modules:

* api - Stash client interfaces and domain objects
* core - base implementation of the Stash REST client (whithout http executor)
* applinks - Atlassian Applink implementation of http executor
* httpclient - Apache HttpClient implementation of http executor
* tests - integration tests against a running Stash instance

Links:

* [Issue tracking](https://bitbucket.org/atlassianlabs/stash-java-client/issues)
* [Continuous Integration](https://server-gdn-bamboo.internal.atlassian.com/browse/SRC-SRCSPECS)

Legal information
=================
The library is licensed under the Apache License, Version 2.0 - see [LICENSE.txt](./LICENSE.txt)

To contribute if you are an Atlassian customer no further action is required because our
[End User Agreement](http://www.atlassian.com/end-user-agreement/) (Section 7) gives Atlassian the right to use
contributions from customers. 
If your are not an Atlassian customer then you will need to sign and submit our [Contribution Agreement](ACLA.pdf).


How to contribute
=================
The source code is stored in a Git repository on Bitbucket. To checkout it run:

    git clone https://bitbucket.org/atlassianlabs/stash-java-client.git

The Stash client is built with Maven 3.0.5 so it's the recommended Maven version however it may work with other versions.

All source code must follow the JLS recommendations and is enforced by the Checkstyle plugin (and [checkstyle.xml](checkstyle.xml)).
Checkstyle is assigned to the 'verify' phase however it can be run with the following command

    mvn checkstyle:checkstyle

To contribute you code to the client library please:
* create a feature branch, 
* change the code, 
* add unit and/or integration tests, 
* test it and 
* eventually create a [pull request](https://bitbucket.org/atlassianlabs/stash-java-client/pull-requests)
to review them. 
After our review, your feature branch will be merged into the master branch!

Atlassian requires contributors to sign a Contributor License Agreement,
known as a CLA. This serves as a record stating that the contributor is
entitled to contribute the code/documentation/translation to the project
and is willing to have it used in distributions and derivative works
(or is willing to transfer ownership).

Prior to accepting your contributions we ask that you please follow the appropriate
link below to digitally sign the CLA. The Corporate CLA is for those who are
contributing as a member of an organization and the individual CLA is for
those contributing as an individual.

See _Contributors License Agreement_ section of [Open Source at Atlassian](https://developer.atlassian.com/opensource/) page.


Testing against a Stash instance
---------------------------------
First we have to start the client library in a plugin inside a host application:

    # install locally all artifacts required by tests
    mvn install -DskipTests -DskipChecks
    mvn -f tests/pom.xml amps:debug -DinstanceId=bamboo

Next in a separate console start the Stash instance in order to test against it:

    mvn -f tests/pom.xml amps:run -DinstanceId=bitbucket
    
or

    mvn -f tests/pom.xml amps:run -DinstanceId=bitbucket -Dstash.or.bitbucket.version=4.12.0

Having two instances up and running you can go to the host application (Bamboo)
[Plugin Test Console](http://localhost:6990/bamboo/plugins/servlet/it-test-console), login as admin/admin,
and press "play" button near any test class (eg. StashClientWiredTest). It should execute "wired tests" from the tests module.
The host application listens on 5005 debug port so you can connect to it and debug the code.


Deploying Bamboo Specs to your own Bamboo Instance
------------------------------
This can be useful if you want to run automated tests against various Bitbucket Server/Stash instances.
Bamboo Specs with various tests for this project can be found in
[StashJavaClientBambooSpec.java](https://bitbucket.org/atlassianlabs/stash-java-client/src/master/stash-java-client-bamboo-specs/src/main/java/com/atlassian/stash/rest/client/bamboo/specs/StashJavaClientBambooSpec.java).

In order to deploy them to your Bamboo Instance, familiarise with the
[Bamboo Specs Tutorial](https://confluence.atlassian.com/bamboo/tutorial-create-a-simple-plan-with-bamboo-specs-894743911.html#Tutorial:CreateasimpleplanwithBambooSpecs-Step5:PublishBambooSpecstotheBambooserver).


Release
------------------------------

https://extranet.atlassian.com/display/RELENG/HOWTO+-+Deploy+Atlassian+Open+Source+Software+to+the+Central+Repository

Steps:
* Install gpg

        sudo port install gnupg gnupg2
    
* Generate gpg key

        gpg --gen-key
        
* Add public key to sks-keyservers.net

       gpg --armor --export ... | pbcopy   
       #Paste to https://sks-keyservers.net/i/
       
* Make sure you have atlassian-central defined in your maven configuration
       
        <server>
            <id>atlassian-central</id>
            <username>...</username>
            <password>...</password>
        </server>
        
* Make sure integration tests are [green](https://server-gdn-bamboo.internal.atlassian.com/browse/SRC-SRCSPECS)
               
* Prepare release

       mvn release:prepare -P skipItTests
       
* Perform release
    
       mvn release:perform -P skipItTests
 
