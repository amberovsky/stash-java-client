package com.atlassian.stash.rest.client.httpclient;

import com.atlassian.stash.rest.client.api.StashClient;
import com.atlassian.stash.rest.client.core.StashClientImpl;

import javax.annotation.Nonnull;

public class HttpClientStashClientFactoryImpl implements HttpClientStashClientFactory {

    @Override
    public StashClient getStashClient(@Nonnull HttpClientConfig config) {
        HttpClientHttpExecutor httpExecutor = new HttpClientHttpExecutor(config);
        return new StashClientImpl(httpExecutor);
    }
}
