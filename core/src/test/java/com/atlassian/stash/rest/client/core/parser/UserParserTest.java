package com.atlassian.stash.rest.client.core.parser;

import static com.atlassian.stash.rest.client.api.EntityMatchers.user;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.Matchers.nullValue;
import static org.junit.Assert.assertThat;

import java.util.Date;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import com.atlassian.stash.rest.client.api.entity.User;
import com.atlassian.stash.rest.client.core.TestData;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

public class UserParserTest {

    @Rule
    public ExpectedException thrown = ExpectedException.none();

    private UserParser userParser;

    private JsonArray values;

    @Before
    public void before() {

        userParser = new UserParser();
        JsonParser jsonParser = new JsonParser();
        JsonObject jo = (JsonObject)jsonParser.parse(TestData.USER_PARSER);
        values = jo.getAsJsonArray("values");
    }

    @Test
    public void testApply() throws Exception  {
        User user = userParser.apply(values.get(0));
        assertThat(user, user()
            .name(is("jcitizen"))
            .emailAddress(is("jane@example.com"))
            .id(is(101L))
            .displayName(is("Jane Citizen"))
            .active(is(true))
            .slug(is("jcitizen"))
            .type(is("NORMAL"))
            .directoryName(is("Bitbucket Internal Directory"))
            .deletable(is(true))
            .lastAuthenticationTimestamp(is(new Date(1368145580548L)))
            .mutableDetails(is(true))
            .mutableGroups(is(true))
            .build()
        );

        user = userParser.apply(values.get(1));
        assertThat(user, user()
            .name(is("foobar"))
            .emailAddress(is("foobar@example.com"))
            .id(is(102L))
            .displayName(is("Foo Bar"))
            .active(is(true))
            .slug(is("foobar"))
            .type(is("NORMAL"))
            .directoryName(is("Bitbucket Internal Directory"))
            .deletable(is(false))
            .lastAuthenticationTimestamp(is(new Date(1368145580578L)))
            .mutableDetails(is(false))
            .mutableGroups(is(false))
            .build()
        );

        user = userParser.apply(values.get(2));
        assertThat(user, user()
            .name(is("ldapUser"))
            .emailAddress(is("ldap.user@example.com"))
            .id(is(103L))
            .displayName(is("user, ldap, example"))
            .active(is(true))
            .slug(is("ldap.user"))
            .type(is("NORMAL"))
            .directoryName(is("Active Directory server"))
            .deletable(is(false))
            .lastAuthenticationTimestamp(is(new Date(1500967032657L)))
            .mutableDetails(is(false))
            .mutableGroups(is(true))
            .selfUrl(is("https://bitbucket.org/users/ldap.user"))
            .build()
        );

        user = userParser.apply(values.get(3));
        assertThat(user, user()
            .name(is("missingAttributes"))
            .emailAddress(nullValue(String.class))
            .id(is(104L))
            .displayName(is("Missing Attributes"))
            .active(is(true))
            .slug(is("missingAttributes"))
            .type(is("NORMAL"))
            .directoryName(nullValue(String.class))
            .deletable(is(false))
            .lastAuthenticationTimestamp(nullValue(Date.class))
            .mutableDetails(is(false))
            .mutableGroups(is(true))
            .selfUrl(nullValue(String.class))
            .build()
        );
    }


    @Test
    public void testApply_missingDelete() throws Exception  {

        User user = userParser.apply(values.get(4));
        assertThat(user, user()
            .name(is("missingDelete"))
            .emailAddress(is("missing.delete@example.com"))
            .id(is(105L))
            .displayName(is("Missing Delete"))
            .active(is(true))
            .slug(is("missingDelete"))
            .type(is("NORMAL"))
            .directoryName(is("Bitbucket Internal Directory"))
            .deletable(is(false))
            .lastAuthenticationTimestamp(is(new Date(1368145580578L)))
            .mutableDetails(is(false))
            .mutableGroups(is(false))
            .build()
        );
    }

}
