package com.atlassian.stash.rest.client.core;

import com.google.common.io.CharStreams;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

public class TestData {
    public static final String REPOS = loadString("repos.json");
    public static final String PROJECTS = loadString("projects.json");
    public static final String PROJECT = loadString("project.json");
    public static final String PROJECT_GROUP_PERMISSIONS = loadString("project-group-permissions.json");
    public static final String PROJECT_GROUP_PERMISSIONS_PARSER = loadString("project-group-permissions-parser.json");
    public static final String PROJECT_USER_PERMISSIONS = loadString("project-user-permissions.json");
    public static final String PROJECT_USER_PERMISSIONS_PARSER = loadString("project-user-permissions-parser.json");
    public static final String REPO_MY_REPO = loadString("repo-my-repo.json");
    public static final String REPO_PERSONAL_FORKED = loadString("repo-personal-forked.json");
    public static final String REPO_CREATE_409 = loadString("repo-create-409.json");
    public static final String REPO_CREATE_401 = loadString("repo-create-401.json");
    public static final String BRANCH = loadString("branch.json");
    public static final String BRANCHES = loadString("branches.json");
    public static final String TAGS = loadString("tags.json");
    public static final String USER_KEYS = loadString("user-keys.json");
    public static final String USER_KEYS_2 = loadString("user-keys-2.json");
    public static final String USER = loadString("user.json");
    public static final String USER_WITHOUT_FILTER = loadString("user-without-filter.json");
    public static final String USER_PARSER = loadString("user-parser.json");
    public static final String PULL_REQ_CREATE_STASH_3_0 = loadString("pull-req-create-stash-3_0_8.json");
    public static final String PULL_REQ_CREATE_BBS_4_11 = loadString("pull-req-create-bbs-4_11_0.json");
    public static final String PULL_REQ_CREATE_ERROR_UNATHORISED = loadString("pull-req-create-error-unathorised.json");
    public static final String PULL_REQ_CREATE_ERROR_DUP = loadString("pull-req-create-error-duplicate.json");
    public static final String PULL_REQS_FOR_BRANCH_STASH_3_0 = loadString("pull-reqs-for-branch-stash-3_0_8.json");
    public static final String PULL_REQS_FOR_BRANCH_STASH_3_3 = loadString("pull-reqs-for-branch-stash-3_3_5.json");
    public static final String PULL_REQS_FOR_BRANCH_BBS_4_0 = loadString("pull-reqs-for-branch-bbs-4_0_8.json");
    public static final String PULL_REQS_FOR_BRANCH_BBS_4_10 = loadString("pull-reqs-for-branch-bbs-4_10_2.json");
    public static final String PULL_REQS_FOR_BRANCH_BBS_4_10_MERGE_CONFLICT = loadString("pull-reqs-for-branch-bbs-4_10_2-merge-conflict.json");
    public static final String MERGE_PULL_REQ = loadString("merge-pull-req.json");
    public static final String MERGE_PULL_REQ_ERROR_OUT_OF_DATE = loadString("merge-pull-req-error-out-of-date.json");
    public static final String CAN_MERGE_CLEAN_BBS_4_9 = loadString("can-merge-clean-bbs-4_9_1.json");
    public static final String CAN_MERGE_CONFLICTED_BBS_4_10 = loadString("can-merge-conflicted-bbs-4_10_2.json");
    public static final String CAN_MERGE_ERROR_PR_DECLINED_BBS_4_10 = loadString("can-merge-error-pr-declined-bbs-4_10_2.json");
    public static final String FORK_REPO = loadString("fork-repo.json");
    public static final String FORK_REPO_ERROR_CONFLICT = loadString("fork-repo-error-conflict.json");
    public static final String ADD_REPO_USER_PERM_ERROR_USER_NOT_FOUND = loadString("add-repo-user-permission-error-user-not-found.json");
    public static final String CREATE_COMMENT = loadString("create-comment.json");
    public static final String APPLICATION_PROPERTIES = loadString("application-properties.json");
    public static final String CREATE_TASK = loadString("create-task.json");
    public static final String CREATE_TASK_FUTURE_PROOF_HYPOTHETICAL_ANCHOR_TYPE = loadString("create-task-future-proof-hypothetical-new-anchor-type.json");
    public static final String CREATE_TASK_ERROR_ANCHOR_NOT_FOUND = loadString("create-task-error-anchor-not-found.json");
    public static final String UPDATE_TASK = loadString("update-task.json");
    public static final String MIRRORS = loadString("mirrors.json");


    private static String loadString(String name) {
        try {
            InputStream inputStream = TestData.class.getResourceAsStream(name);
            if (inputStream == null) {
                throw new FileNotFoundException(name);
            }
            return CharStreams.toString(new InputStreamReader(inputStream, "UTF-8"));
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
}
