package com.atlassian.stash.rest.client.core.parser;

import static com.atlassian.stash.rest.client.api.EntityMatchers.projectGroupPermission;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.nullValue;
import static org.junit.Assert.assertThat;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import com.atlassian.stash.rest.client.api.entity.ProjectGroupPermission;
import com.atlassian.stash.rest.client.api.entity.ProjectPermission;
import com.atlassian.stash.rest.client.core.TestData;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

public class ProjectGroupPermissionParserTest {

    @Rule
    public ExpectedException thrown = ExpectedException.none();

    private ProjectGroupPermissionParser pgpp;

    private JsonArray values;

    @Before
    public void before() {

        pgpp = new ProjectGroupPermissionParser();
        JsonParser jsonParser = new JsonParser();
        JsonObject jo = (JsonObject)jsonParser.parse(TestData.PROJECT_GROUP_PERMISSIONS_PARSER);
        values = jo.getAsJsonArray("values");

    }

    @Test
    public void testApply() throws Exception {

        ProjectGroupPermission projectGroupPermission = pgpp.apply(values.get(0));
        assertThat(projectGroupPermission, projectGroupPermission()
            .groupName(is("READ"))
            .permission(is(ProjectPermission.PROJECT_READ))
            .build()
        );

        projectGroupPermission = pgpp.apply(values.get(1));
        assertThat(projectGroupPermission, projectGroupPermission()
            .groupName(is("WRITE"))
            .permission(is(ProjectPermission.PROJECT_WRITE))
            .build()
        );

        projectGroupPermission = pgpp.apply(values.get(2));
        assertThat(projectGroupPermission, projectGroupPermission()
            .groupName(is("ADMIN"))
            .permission(is(ProjectPermission.PROJECT_ADMIN))
            .build()
        );

        projectGroupPermission = pgpp.apply(values.get(3));
        assertThat(projectGroupPermission, projectGroupPermission()
            .groupName(is("UNKNOWN"))
            .permission(nullValue())
            .build()
        );

    }


    @Test
    public void testApply_NoUserElement() throws Exception {

        thrown.expect(NullPointerException.class);
        pgpp.apply(values.get(4));

    }

    @Test
    public void testApply_NoUserNameElement() throws Exception {

        thrown.expect(NullPointerException.class);
        pgpp.apply(values.get(5));
    }


}
