package com.atlassian.stash.rest.client.core.entity;


import com.google.gson.JsonObject;

public class StashRepositorySshKeyRequest {
    private final String projectName;
    private final String repositorySlug;
    private final String label;
    private final String publicKey;
    private final String permission;

    public StashRepositorySshKeyRequest(final String projectName, final String repositorySlug, final String label, final String publicKey, final String permission) {
        this.projectName = projectName;
        this.repositorySlug = repositorySlug;
        this.label = label;
        this.publicKey = publicKey;
        this.permission = permission;
    }

    public JsonObject toJson() {
        JsonObject key = new JsonObject();
        key.addProperty("id", 0);
        key.addProperty("label", label);
        key.addProperty("text", publicKey);

        JsonObject keyPayload = new JsonObject();
        keyPayload.addProperty("project", projectName);
        keyPayload.addProperty("repository", repositorySlug);
        keyPayload.addProperty("permission", permission);
        keyPayload.add("key", key);
        return keyPayload;
    }
}
