package com.atlassian.stash.rest.client.core.parser;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

import com.atlassian.stash.rest.client.api.entity.ProjectUserPermission;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

public class ProjectUserPermissionParser extends ProjectPermissionParser<ProjectUserPermission> {

    @Nonnull
    @Override
    public ProjectUserPermission apply(@Nullable final JsonElement pgPermission)  {

        final JsonObject jsonObject = getPgPermission(pgPermission);

        final String userName = getNameAttributeFromObject(jsonObject, "user");
        final String permissionRaw = jsonObject.get("permission").getAsString();

        return new ProjectUserPermission(userName, mapProjectPermission(permissionRaw));
    }
}
