package com.atlassian.stash.rest.client.core.entity;

import com.google.common.base.Objects;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

public class Link {
    @Nonnull
    private final String href;
    @Nullable
    private final String name;

    public Link(@Nonnull String href, @Nullable String name) {
        this.href = href;
        this.name = name;
    }

    @Nullable
    public String getName() {
        return name;
    }

    @Nonnull
    public String getHref() {
        return href;
    }

    @Override
    public String toString() {
        return Objects.toStringHelper(this)
                .add("href", href)
                .add("name", name)
                .toString();
    }
}
