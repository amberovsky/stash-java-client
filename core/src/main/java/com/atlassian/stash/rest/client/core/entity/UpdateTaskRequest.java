package com.atlassian.stash.rest.client.core.entity;

import com.atlassian.stash.rest.client.api.entity.TaskState;
import com.google.common.base.Preconditions;
import com.google.gson.JsonObject;

import javax.annotation.Nullable;

public class UpdateTaskRequest {
    @Nullable
    private final TaskState state;
    @Nullable
    private final String text;

    public UpdateTaskRequest(@Nullable final TaskState state, @Nullable final String text) {
        Preconditions.checkArgument((null != state) || (null != text),
                "at least one of state and text need to be provided");
        this.state = state;
        this.text = text;
    }

    public JsonObject toJson() {
        final JsonObject req = new JsonObject();

        if (null != state) {
            req.addProperty("state", state.name());
        }
        if (null != text) {
            req.addProperty("text", text);
        }

        return req;
    }
}
