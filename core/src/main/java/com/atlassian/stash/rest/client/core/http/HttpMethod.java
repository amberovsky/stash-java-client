package com.atlassian.stash.rest.client.core.http;

public enum HttpMethod {
    GET,
    POST,
    DELETE,
    PUT
}
