package com.atlassian.stash.rest.client.core.entity;

import com.google.gson.JsonObject;

import javax.annotation.Nonnull;

public class CreateCommentRequest {
    @Nonnull
    public final String text;

    public CreateCommentRequest(@Nonnull final String text) {
        this.text = text;
    }

    public JsonObject toJson() {
        final JsonObject req = new JsonObject();

        req.addProperty("text", text);

        return req;
    }
}