package com.atlassian.stash.rest.client.bamboo.specs;

import com.atlassian.bamboo.specs.api.BambooSpec;
import com.atlassian.bamboo.specs.api.builders.BambooKey;
import com.atlassian.bamboo.specs.api.builders.notification.Notification;
import com.atlassian.bamboo.specs.api.builders.permission.PermissionType;
import com.atlassian.bamboo.specs.api.builders.permission.Permissions;
import com.atlassian.bamboo.specs.api.builders.permission.PlanPermissions;
import com.atlassian.bamboo.specs.api.builders.plan.Job;
import com.atlassian.bamboo.specs.api.builders.plan.Plan;
import com.atlassian.bamboo.specs.api.builders.plan.Stage;
import com.atlassian.bamboo.specs.api.builders.plan.artifact.Artifact;
import com.atlassian.bamboo.specs.api.builders.plan.branches.BranchCleanup;
import com.atlassian.bamboo.specs.api.builders.plan.branches.PlanBranchManagement;
import com.atlassian.bamboo.specs.api.builders.project.Project;
import com.atlassian.bamboo.specs.api.builders.requirement.Requirement;
import com.atlassian.bamboo.specs.builders.notification.PlanCompletedNotification;
import com.atlassian.bamboo.specs.builders.notification.ResponsibleRecipient;
import com.atlassian.bamboo.specs.builders.task.CheckoutItem;
import com.atlassian.bamboo.specs.builders.task.MavenTask;
import com.atlassian.bamboo.specs.builders.task.ScriptTask;
import com.atlassian.bamboo.specs.builders.task.VcsCheckoutTask;
import com.atlassian.bamboo.specs.builders.trigger.RepositoryPollingTrigger;
import com.atlassian.bamboo.specs.model.task.ScriptTaskProperties;
import com.atlassian.bamboo.specs.util.BambooServer;

import javax.annotation.Nonnull;
import java.util.Arrays;

@BambooSpec
public class StashJavaClientBambooSpec {

    static final Project BAMBOO_PROJECT = new Project()
            .key(new BambooKey("SRC"))
            .name("stash-java-client");
    private static final String CHECKOUT_PATH = "stash-java-client";
    private static final String RELEASE_TOOLS_CHECKOUT_PATH = CHECKOUT_PATH + "/release";
    private static final String JDK_VERSION = "JDK 1.8";
    private static final String MAVEN_EXECUTABLE = "mvnvm";
    private static final String STASH_JAVA_CLIENT_REPO = "stash-java-client";
    private static final String JAVA_PLATFORM_RELEASE_SCRIPTS_REPO = "java-platform-release-scripts";

    @SuppressWarnings("unused")
    enum IntegrationTestTargetProduct {
        // BBS 5.0+ won't start, there is problem with AMPS: https://bitbucket.org/atlassian/amps/pull-requests/264/ampsdev-209-support-bitbucket-server-5/activity
        // upgrading AMPS to 6.3.0 is not sufficient, I can see further problems in bitbucket.log
        // See also https://community.atlassian.com/t5/Bitbucket-questions/Atlas-run-no-longer-works-with-Bitbucket-Server-5-0-0/qaq-p/584083
        BITBUCKET_5_2_2("5.2.2", false),
        BITBUCKET_5_1_4("5.1.4", false),
        BITBUCKET_5_0_6("5.0.6", false),
        BITBUCKET_4_14_7("4.14.7", true),
        BITBUCKET_4_13_1("4.13.1", false),
        // 4.12.0 won"t start with Git 2.11+, see https://jira.atlassian.com/browse/BSERV-9388
        BITBUCKET_4_12_0("4.12.0", false),
        BITBUCKET_4_11_2("4.11.2", false),
        // PR Merge Status added in BBS 4.10.0
        BITBUCKET_4_10_2("4.10.2", true),
        BITBUCKET_4_9_1("4.9.1", true),
        BITBUCKET_4_8_6("4.8.6", false),
        BITBUCKET_4_7_2("4.7.2", false),
        BITBUCKET_4_6_4("4.6.4", false),
        BITBUCKET_4_5_3("4.5.3", false),
        BITBUCKET_4_4_4("4.4.4", false),
        BITBUCKET_4_3_3("4.3.3", false),
        BITBUCKET_4_2_3("4.2.3", false),
        BITBUCKET_4_1_6("4.1.6", false),
        // Stash renamed to BBS in 4.0.0
        BITBUCKET_4_0_8("4.0.8", true),
        STASH_3_11_6("3.11.6", true),
        STASH_3_10_4("3.10.4", false),
        STASH_3_9_2("3.9.2", false),
        STASH_3_8_1("3.8.1", false),
        STASH_3_7_4("3.7.4", false),
        STASH_3_6_1("3.6.1", false),
        STASH_3_5_1("3.5.1", false),
        STASH_3_4_5("3.4.5", false),
        // task support added in 3.3.0
        STASH_3_3_5("3.3.5", true),
        STASH_3_2_7("3.2.7", true),
        STASH_3_1_7("3.1.7", false),
        STASH_3_0_8("3.0.8", false),
        STASH_2_11_3("2.11.3", false);

        private final String productVersion;
        private final boolean enabled;

        IntegrationTestTargetProduct(final String productVersion, final boolean enabled) {
            this.productVersion = productVersion;
            this.enabled = enabled;
        }

        String getProductName() {
            return productVersion.compareTo("4.0.0") < 0 ? "stash" : "bitbucket";
        }

        public String getProductVersion() {
            return productVersion;
        }

        public boolean isEnabled() {
            return enabled;
        }
    }

    public static void main(String... argv) {
        //By default credentials are read from the '.credentials' file.
        BambooServer bambooServer = new BambooServer("https://server-gdn-bamboo.internal.atlassian.com");

        new StashJavaClientBambooSpec().buildAndPublishPlanSpecs(bambooServer);
    }

    void buildAndPublishPlanSpecs(final BambooServer bambooServer) {
        Plan plan = buildPlan();

        bambooServer.publish(plan);

        PlanPermissions planPermission = buildPlanPermission(plan);

        bambooServer.publish(planPermission);
    }

    Plan buildPlan() {
        return new Plan(BAMBOO_PROJECT,
                "stash-java-client Bamboo Specs",
                new BambooKey("SRCSPECS"))
                .description("Plan created from https://bitbucket.org/atlassianlabs/stash-java-client/src/stash-java-client-bamboo-specs/")
                .stages(buildFastChecksStage(), buildIntegrationTestsStage(), buildReleaseStage())
                // define global linked repository on Bamboo instance first
                .linkedRepositories(STASH_JAVA_CLIENT_REPO, JAVA_PLATFORM_RELEASE_SCRIPTS_REPO)
                .triggers(new RepositoryPollingTrigger()
                        .description("Polling repo every 3 minutes"))
                .planBranchManagement(new PlanBranchManagement()
                        .createForVcsBranchMatching(".*")
                        .delete(new BranchCleanup()
                                .whenRemovedFromRepositoryAfterDays(30)
                                .whenInactiveInRepositoryAfterDays(30))
                        .notificationForCommitters())
                .notifications(new Notification()
                        .type(new PlanCompletedNotification())
                        .recipients(new ResponsibleRecipient()));
    }

    private Stage buildReleaseStage() {
        return new Stage("Release")
                .manual(true)
                .description("Releases new stash-java-client version")
                .jobs(buildReleaseJob());
    }

    private Job buildReleaseJob() {
        return new Job("Release", new BambooKey("RELEASE"))
                .description("Releases new stash-java-client version")
                .tasks(buildCheckoutDefaultRepositoryTask(),
                        buildCheckoutReleaseToolsRepoTask(),
                        buildReleaseScriptTask())
                .artifacts(new Artifact()
                        .name("Project Effective Pom")
                        .copyPattern("effective-pom.xml")
                        .location(RELEASE_TOOLS_CHECKOUT_PATH))
                .requirements(new Requirement("system.jdk." + JDK_VERSION),
                        new Requirement("system.git.executable"),
                        new Requirement("os")
                                .matchValue("Linux")
                                .matchType(Requirement.MatchType.EQUALS),
                        new Requirement("system.builder.mvn3." + MAVEN_EXECUTABLE));
    }

    private ScriptTask buildReleaseScriptTask() {
        return new ScriptTask()
                .description("Release on a branch and merge back")
                .workingSubdirectory(CHECKOUT_PATH)
                .interpreter(ScriptTaskProperties.Interpreter.BINSH_OR_CMDEXE)
                .location(ScriptTaskProperties.Location.FILE)
                .fileFromPath("./release/bin/autorelease.sh")
                .argument("-m:${bamboo.capability.system.builder.mvn3." + MAVEN_EXECUTABLE + "}/bin/mvn" +
                        " -j:${bamboo.capability.system.jdk." + JDK_VERSION + "} -s:master");
    }

    private VcsCheckoutTask buildCheckoutReleaseToolsRepoTask() {
        return new VcsCheckoutTask()
                .description("Checkout release tools")
                .checkoutItems(new CheckoutItem()
                        .repository(JAVA_PLATFORM_RELEASE_SCRIPTS_REPO)
                        .path(RELEASE_TOOLS_CHECKOUT_PATH));
    }

    private Stage buildIntegrationTestsStage() {
        return new Stage("Integration Tests")
                .jobs(Arrays.stream(IntegrationTestTargetProduct.values())
                        .map(this::buildIntegrationTestJob)
                        .toArray(Job[]::new));
    }

    @Nonnull
    private Stage buildFastChecksStage() {
        return new Stage("Fast checks")
                .jobs(buildCheckStyleJob(), buildUnitTestsJob());
    }

    Job buildIntegrationTestJob(IntegrationTestTargetProduct target) {
        final String jobName = "it-" + target.getProductName() + "-" + target.getProductVersion();
        final String jobKey = "IT" + target.getProductName().toUpperCase() + target.getProductVersion().replaceAll("\\.", "");
        final String testGroups = target.getProductVersion().compareTo("4.1.6") <= 0 ?
                " -DtestGroups=stash-integration-tests " : "";
        return new Job(jobName,
                new BambooKey(jobKey))
                .description("Integration tests with " + target.getProductName() + " " + target.getProductVersion())
                .enabled(target.isEnabled())
                .artifacts(new Artifact()
                                .name(target.getProductName() + " log")
                                .copyPattern(target.getProductName() + ".log")
                                .location(CHECKOUT_PATH + "/tests/target"),
                        new Artifact()
                                .name("bamboo log")
                                .copyPattern("bamboo.log")
                                .location(CHECKOUT_PATH + "/tests/target"),
                        new Artifact()
                                .name("tests module test results")
                                .copyPattern("**/TEST*.xml")
                                .location(CHECKOUT_PATH + "/tests/target/group-stash-integration-tests/"))
                .tasks(buildCheckoutDefaultRepositoryTask(),
                        new MavenTask()
                                .description("Install artifacts")
                                .goal("clean install -DskipTests -DskipChecks -B")
                                .jdk(JDK_VERSION)
                                .executableLabel(MAVEN_EXECUTABLE)
                                .workingSubdirectory(CHECKOUT_PATH),
                        new MavenTask()
                                .description("Integration tests using " + target.getProductName() + " instance")
                                .goal("integration-test -Dstash.or.bitbucket.product=" + target.getProductName() +
                                        " -Dstash.or.bitbucket.version=" + target.getProductVersion() + testGroups +
                                        " -pl tests -B")
                                .jdk(JDK_VERSION)
                                .executableLabel(MAVEN_EXECUTABLE)
                                .hasTests(true)
                                .testResultsPath("**/target/**/surefire-reports/*.xml")
                                .workingSubdirectory(CHECKOUT_PATH));
    }

    private Job buildCheckStyleJob() {
        return new Job("checkstyle-findbugs",
                new BambooKey("CHEC"))
                .tasks(buildCheckoutDefaultRepositoryTask(),
                        new MavenTask()
                                .description("Run checkstyle and findbugs as part of verify")
                                .goal("clean verify -DskipTests -B")
                                .jdk(JDK_VERSION)
                                .executableLabel(MAVEN_EXECUTABLE)
                                .workingSubdirectory(CHECKOUT_PATH));
    }

    private Job buildUnitTestsJob() {
        return new Job("ut",
                new BambooKey("UT"))
                .description("Unit Tests")
                .artifacts(new Artifact()
                                .name("core module test results")
                                .copyPattern("TEST*.xml")
                                .location(CHECKOUT_PATH + "/core/target/surefire-reports"),
                        new Artifact()
                                .name("applinks module test results")
                                .copyPattern("TEST*.xml")
                                .location(CHECKOUT_PATH + "/applinks/target/surefire-reports/"))
                .tasks(buildCheckoutDefaultRepositoryTask(),
                        new MavenTask()
                                .description("Unit tests")
                                .goal("clean package -B")
                                .jdk(JDK_VERSION)
                                .executableLabel(MAVEN_EXECUTABLE)
                                .hasTests(true)
                                .testResultsPath("**/target/**/surefire-reports/*.xml")
                                .workingSubdirectory(CHECKOUT_PATH));
    }

    private VcsCheckoutTask buildCheckoutDefaultRepositoryTask() {
        return new VcsCheckoutTask()
                .description("Checkout stash-java-client repo")
                .checkoutItems(new CheckoutItem()
                        .defaultRepository()
                        .path(CHECKOUT_PATH));
    }

    @Nonnull
    private PlanPermissions buildPlanPermission(final Plan plan) {
        return new PlanPermissions(plan.getIdentifier())
                .permissions(new Permissions()
                        .groupPermissions("atlassian-dev", PermissionType.VIEW, PermissionType.EDIT, PermissionType.BUILD, PermissionType.CLONE, PermissionType.ADMIN)
                        .loggedInUserPermissions(PermissionType.VIEW)
                        .anonymousUserPermissionView());
    }
}