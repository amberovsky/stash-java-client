package com.atlassian.stash.rest.client.bamboo.specs;

import com.atlassian.bamboo.specs.api.builders.plan.Job;
import com.atlassian.bamboo.specs.api.builders.plan.Plan;
import com.atlassian.bamboo.specs.api.exceptions.PropertiesValidationException;
import com.atlassian.bamboo.specs.api.model.plan.JobProperties;
import com.atlassian.bamboo.specs.api.util.EntityPropertiesBuilders;
import org.junit.Before;
import org.junit.Test;

import static com.atlassian.stash.rest.client.bamboo.specs.StashJavaClientBambooSpec.IntegrationTestTargetProduct.BITBUCKET_4_0_8;
import static com.atlassian.stash.rest.client.bamboo.specs.StashJavaClientBambooSpec.IntegrationTestTargetProduct.BITBUCKET_4_13_1;
import static com.atlassian.stash.rest.client.bamboo.specs.StashJavaClientBambooSpec.IntegrationTestTargetProduct.STASH_3_11_6;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.junit.Assert.assertThat;

public class StashJavaClientBambooSpecTest {
    private StashJavaClientBambooSpec spec;

    @Before
    public void setUp() throws Exception {
        spec = new StashJavaClientBambooSpec();
    }

    @Test
    public void checkYourPlanOffline() throws PropertiesValidationException {
        final Plan plan = spec.buildPlan();

        EntityPropertiesBuilders.build(plan);
    }

    @Test
    public void testIntegrationTestsProductName() throws Exception {
        assertThat(BITBUCKET_4_13_1.getProductName(), equalTo("bitbucket"));
        assertThat(BITBUCKET_4_0_8.getProductName(), equalTo("bitbucket"));
        assertThat(STASH_3_11_6.getProductName(), equalTo("stash"));
    }

    @Test
    public void testBuildIntegrationTestJob() throws Exception {
        final Job job = spec.buildIntegrationTestJob(BITBUCKET_4_0_8);
        final JobProperties jobProps = EntityPropertiesBuilders.build(job);

        assertThat("job key", jobProps.getKey().getKey(), equalTo("ITBITBUCKET408"));
        assertThat("job name", jobProps.getName(), equalTo("it-bitbucket-4.0.8"));
    }
}
