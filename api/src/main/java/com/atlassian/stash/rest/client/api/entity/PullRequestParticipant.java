package com.atlassian.stash.rest.client.api.entity;

import com.google.common.base.Objects;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

/**
 * pull request participant
 */
public class PullRequestParticipant {
    @Nonnull
    private final String name;
    @Nonnull
    private final String displayName;
    @Nonnull
    private final String slug;
    @Nullable
    private final String avatarUrl;
    private final boolean approved;
    @Nullable
    private final String status;
    @Nonnull
    private final String role;

    public PullRequestParticipant(@Nonnull final String name, @Nonnull final String displayName,
                                  @Nonnull final String slug, @Nullable final String avatarUrl,
                                  final boolean approved, @Nullable final String status,
                                  @Nonnull final String role) {
        this.name = name;
        this.displayName = displayName;
        this.slug = slug;
        this.avatarUrl = avatarUrl;
        this.approved = approved;
        this.status = status;
        this.role = role;
    }

    /**
     * @return user name
     */
    public String getName() {
        return name;
    }

    /**
     * @return display name
     */
    public String getDisplayName() {
        return displayName;
    }

    /**
     * @return slug
     */
    public String getSlug() {
        return slug;
    }

    /**
     * @return avatar url
     */
    public String getAvatarUrl() {
        return avatarUrl;
    }

    public boolean isApproved() {
        return approved;
    }

    /**
     * @return review status for this participant in the pull request. As of Bitbucket 4.12.0 status may be any of
     * <code>UNAPPROVED</code>, <code>APPROVED</code> or <code>NEEDS_WORK</code>
     */
    @Nullable
    public String getStatus() {
        return status;
    }

    /**
     * @return role in the given pull request.  As of Bitbucket 4.12.0 role can be any of <code>AUTHOR</code>,
     * <code>REVIEWER</code> or <code>PARTICIPANT</code>
     */
    public String getRole() {
        return role;
    }

    @Override
    public String toString() {
        return Objects.toStringHelper(this)
                .add("name", name)
                .add("displayName", displayName)
                .add("slug", slug)
                .add("avatarUrl", avatarUrl)
                .add("approved", approved)
                .add("status", status)
                .add("role", role)
                .toString();
    }
}
