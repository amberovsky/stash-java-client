package com.atlassian.stash.rest.client.api.entity;

import com.google.common.base.Objects;

import javax.annotation.Nonnull;

/**
 * Basic representation of a comment, only few attributes for now.
 */
public class Comment implements TaskAnchor {
    private final long id;
    private final long version;
    @Nonnull
    private final String text;

    public Comment(long id, long version, @Nonnull String text) {
        this.id = id;
        this.version = version;
        this.text = text;
    }

    /**
     * @return comment id
     */
    @Override
    public long getId() {
        return id;
    }

    /**
     * @return comment version
     */
    public long getVersion() {
        return version;
    }

    /**
     * @return comment text
     */
    @Nonnull
    public String getText() {
        return text;
    }

    @Override
    public String toString() {
        return Objects.toStringHelper(this)
                .add("id", id)
                .add("version", version)
                .add("text", text)
                .toString();
    }
}