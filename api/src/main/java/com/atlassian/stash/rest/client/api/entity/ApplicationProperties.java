package com.atlassian.stash.rest.client.api.entity;

import com.google.common.base.Objects;

import javax.annotation.Nonnull;

/**
 * Describes version information and other application properties.
 */
public class ApplicationProperties {
    @Nonnull
    private final String version;
    @Nonnull
    private final String buildNumber;
    @Nonnull
    private final String buildDate;
    @Nonnull
    private final String displayName;

    public ApplicationProperties(@Nonnull final String version, @Nonnull final String buildNumber,
                                 @Nonnull final String buildDate, @Nonnull final String displayName) {
        this.version = version;
        this.buildNumber = buildNumber;
        this.buildDate = buildDate;
        this.displayName = displayName;
    }

    /**
     * @return remote Stash/Bitbucket version, e.g. <code>3.3.5</code>
     */
    @Nonnull
    public String getVersion() {
        return version;
    }

    /**
     * @return remote Stash/Bitbucket build number, e.g. <code>3003005</code>
     */
    @Nonnull
    public String getBuildNumber() {
        return buildNumber;
    }

    /**
     * @return remote Stash/Bitbucket build date, e.g. <code>1419151242926</code>
     */
    public String getBuildDate() {
        return buildDate;
    }

    /**
     * @return remote Stash/Bitbucket application display name, e.g. <code>Stash</code>
     */
    @Nonnull
    public String getDisplayName() {
        return displayName;
    }

    @Override
    public String toString() {
        return Objects.toStringHelper(this)
                .add("version", version)
                .add("buildNumber", buildNumber)
                .add("buildDate", buildDate)
                .add("displayName", displayName)
                .toString();
    }
}