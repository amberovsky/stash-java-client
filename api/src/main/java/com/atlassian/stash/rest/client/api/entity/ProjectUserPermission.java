package com.atlassian.stash.rest.client.api.entity;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

/**
 * Permission of a user on a project. Used for getProjectUserPermissions.
 */
public class ProjectUserPermission {

    @Nonnull
    private final String userName;

    @Nullable
    private final ProjectPermission permission;

    public ProjectUserPermission(@Nonnull final String userName, @Nullable ProjectPermission permission) {
        this.userName = userName;
        this.permission = permission;
    }

    @Nonnull
    public String getUserName() {
        return userName;
    }

    @Nullable
    public ProjectPermission getPermission() {
        return permission;
    }

}
