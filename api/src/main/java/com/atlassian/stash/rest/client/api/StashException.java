package com.atlassian.stash.rest.client.api;

import java.util.Collections;
import java.util.List;

/**
 * Generic exception caused by Stash client. No other exception types should be thrown.
 *
 * <p>
 * Exception may be triggered by many errors so it accepts list of errors
 */
public class StashException extends RuntimeException {
    private List<StashError> errors;

    public StashException(String message) {
        super(message);
        this.errors = toErrors(getMessage(), this);
    }

    public StashException(String message, Throwable cause) {
        super(message, cause);
        this.errors = toErrors(getMessage(), cause);
    }

    public StashException(Throwable cause) {
        super(cause);
        this.errors = toErrors(getMessage(), cause);
    }

    public StashException(List<StashError> errors) {
        super(errors.isEmpty() ? null : errors.get(0).getMessage());
        this.errors = errors;
    }

    /**
     * @return errors which triggered exception
     */
    public List<StashError> getErrors() {
        return errors;
    }

    public static List<StashError> toErrors(String message) {
        return toErrors(message, null);
    }

    public static List<StashError> toErrors(String message, Throwable e) {
        return Collections.singletonList(new StashError(message, null, e != null ? e.getClass().getSimpleName() : null));
    }
}
