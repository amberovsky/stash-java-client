package com.atlassian.stash.rest.client.api.entity;

import com.google.common.base.Objects;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

/**
 * represents repository ref
 */
public class PullRequestRef {
    @Nonnull
    private final String repositorySlug;
    @Nonnull
    private final String repositoryName;
    @Nonnull
    private final String projectKey;
    @Nonnull
    private final String projectName;
    @Nonnull
    private final String id;
    @Nullable
    private final String displayId;

    public PullRequestRef(@Nonnull final String repositorySlug, @Nonnull final String repositoryName,
                          @Nonnull final String projectKey, @Nonnull final String projectName,
                          @Nonnull final String id) {
        this(repositorySlug, repositoryName, projectKey, projectName, id, null);
    }

    public PullRequestRef(@Nonnull final String repositorySlug, @Nonnull final String repositoryName,
                          @Nonnull final String projectKey, @Nonnull final String projectName,
                          @Nonnull final String id, @Nullable final String displayId) {
        this.repositorySlug = repositorySlug;
        this.repositoryName = repositoryName;
        this.projectKey = projectKey;
        this.projectName = projectName;
        this.id = id;
        this.displayId = displayId;
    }

    /**
     * @return repository slug
     */
    @Nonnull
    public String getRepositorySlug() {
        return repositorySlug;
    }

    /**
     * @return repository name
     */
    @Nonnull
    public String getRepositoryName() {
        return repositoryName;
    }

    /**
     * @return project key
     */
    @Nonnull
    public String getProjectKey() {
        return projectKey;
    }

    /**
     * @return project name
     */
    @Nonnull
    public String getProjectName() {
        return projectName;
    }

    /**
     * @return id of a ref, e.g. <code>refs/heads/master</code>
     */
    @Nonnull
    public String getId() {
        return id;
    }

    /**
     * @return An identifier for this reference suitable for display to end users.
     */
    @Nullable
    public String getDisplayId() {
        return displayId;
    }

    @Override
    public String toString() {
        return Objects.toStringHelper(this)
                .add("repositorySlug", repositorySlug)
                .add("repositoryName", repositoryName)
                .add("projectKey", projectKey)
                .add("projectName", projectName)
                .add("id", id)
                .add("displayId", displayId)
                .toString();
    }
}
