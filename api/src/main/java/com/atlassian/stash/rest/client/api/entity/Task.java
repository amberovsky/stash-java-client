package com.atlassian.stash.rest.client.api.entity;

import com.google.common.base.Objects;

import javax.annotation.Nonnull;

public class Task {
    private final long id;
    @Nonnull
    private final String text;
    @Nonnull
    private final String state;
    @Nonnull
    private final TaskAnchor anchor;

    public Task(final long id, @Nonnull final String text, @Nonnull final String state,
                @Nonnull final TaskAnchor anchor) {
        this.id = id;
        this.text = text;
        this.state = state;
        this.anchor = anchor;
    }

    /**
     * @return the task's ID, which is globally unique
     */
    public long getId() {
        return id;
    }

    /**
     * @return the raw text of the task (limited to 32k)
     */
    @Nonnull
    public String getText() {
        return text;
    }

    /**
     * @return the state (e.g. <code>OPEN</code> or <code>RESOLVED</code>) of the task
     */
    @Nonnull
    public String getState() {
        return state;
    }

    /**
     * @return the anchor of the task, such as a pull request's comment
     */
    @Nonnull
    public TaskAnchor getAnchor() {
        return anchor;
    }

    @Override
    public String toString() {
        return Objects.toStringHelper(this)
                .add("id", id)
                .add("text", text)
                .add("state", state)
                .add("anchor", anchor)
                .toString();
    }
}
