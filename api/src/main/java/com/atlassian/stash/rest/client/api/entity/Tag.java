package com.atlassian.stash.rest.client.api.entity;

import com.google.common.base.Objects;

/**
 * Describes a repository tag
 *
 * @see <a href="https://developer.atlassian.com/static/rest/stash/3.11.3/stash-rest.html#idp663664">Stash documentation: tags</a>
 */
public class Tag {
    private final String id;
    private final String displayId;
    private final String latestChangeset;
    private final String latestCommit;
    private final String hash;


    public Tag(final String id, final String displayId, final String latestChangeset, String latestCommit, String hash) {
        this.id = id;
        this.displayId = displayId;
        this.latestChangeset = latestChangeset;
        this.latestCommit = latestCommit;
        this.hash = hash;
    }

    /**
     * @return fully qualified branch identifier eg. refs/tags/release-2.0.0
     */
    public String getId() {
        return id;
    }

    /**
     * @return tag display identifier eg. release-2.0.0
     */
    public String getDisplayId() {
        return displayId;
    }

    /**
     * @return tag head changeset
     */
    public String getLatestChangeset() {
        return latestChangeset;
    }

    /**
     * @return tag head commit
     */
    public String getLatestCommit() {
        return latestCommit;
    }

    /**
     * @return tag head hash
     */
    public String getHash() {
        return hash;
    }

    @Override
    public String toString() {
        return Objects.toStringHelper(this)
                .add("id", id)
                .add("displayId", displayId)
                .add("latestChangeset", latestChangeset)
                .add("latestCommit", latestCommit)
                .add("hash", hash)
                .toString();
    }
}
