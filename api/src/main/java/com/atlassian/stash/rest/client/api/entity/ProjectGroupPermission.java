package com.atlassian.stash.rest.client.api.entity;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

/**
 * Permission of a group on a project. Used for getProjectGroupPermissions.
 */
public class ProjectGroupPermission {

    @Nonnull
    private final String groupName;
    @Nullable
    private final ProjectPermission permission;

    public ProjectGroupPermission(@Nonnull final String groupName, @Nullable ProjectPermission permission) {
        this.groupName = groupName;
        this.permission = permission;
    }

    /**
     * @return group name
     */
    public String getGroupName() {
        return groupName;
    }

    /**
     * @return The permission of the group on the associated project
     */
    public ProjectPermission getPermission() {
        return permission;
    }

}
