package com.atlassian.stash.rest.client.api.entity;

import com.google.common.base.Objects;
import com.google.common.collect.ImmutableList;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.util.List;

/**
 * Describes a page of values.
 *
 * <a href="https://developer.atlassian.com/static/rest/stash/2.11.3/stash-rest.html#paging-params">Stash documentation</a>:
 * "Stash uses paging to conserve server resources and limit response size for resources
 * that return potentially large collections of items. A request to a paged API will result in a values array
 * wrapped in [..] object with some paging metadata"
 *
 * @param <T> Value type
 *
 * @see <a href="https://developer.atlassian.com/static/rest/stash/2.11.3/stash-rest.html#paging-params">Stash documentation</a>
 */
public class Page<T> {
    private final int size;
    private final int limit;
    private final boolean lastPage;
    private final int start;
    @Nullable
    private final Integer nextPageStart;
    @Nonnull
    private final List<T> values;

    public Page(final int size, final int limit, final boolean lastPage, final int start,
                @Nullable final Integer nextPageStart, @Nonnull final Iterable<T> values) {
        this.size = size;
        this.limit = limit;
        this.lastPage = lastPage;
        this.values = ImmutableList.copyOf(values);
        this.start = start;
        this.nextPageStart = nextPageStart;
    }

    /**
     * @return number of values
     */
    public int getSize() {
        return size;
    }

    /**
     * @return requested number of values
     */
    public int getLimit() {
        return limit;
    }

    /**
     * @return true if current page is the last one ie. there are no more values
     */
    public boolean isLastPage() {
        return lastPage;
    }

    /**
     * @return value list
     */
    @Nonnull
    public List<T> getValues() {
        return values;
    }

    /**
     * @return requested position of first value
     */
    public int getStart() {
        return start;
    }

    /**
     * @return suggested position of first value for next page or null if there are no more values
     */
    @Nullable
    public Integer getNextPageStart() {
        return nextPageStart;
    }

    @Override
    public String toString() {
        return Objects.toStringHelper(this)
                .add("size", size)
                .add("limit", limit)
                .add("lastPage", lastPage)
                .add("start", start)
                .add("nextPageStart", nextPageStart)
                .add("values", values)
                .toString();
    }
}
