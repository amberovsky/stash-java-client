package com.atlassian.stash.rest.client.api;

import javax.annotation.Nullable;
import java.util.List;

/**
 * Exception caused by REST communication (HTTP) errors eg. 400
 */
public class StashRestException extends StashException {
    private final int statusCode;
    private final String statusMessage;
    private final String responseBody;

    public StashRestException(String message, int statusCode, String statusMessage) {
        super(message);
        this.statusCode = statusCode;
        this.statusMessage = statusMessage;
        this.responseBody = null;
    }

    public StashRestException(List<StashError> errors, int statusCode, String statusMessage) {
        super(errors);
        this.statusCode = statusCode;
        this.statusMessage = statusMessage;
        this.responseBody = null;
    }

    public StashRestException(List<StashError> errors, int statusCode, String statusMessage, String responseBody) {
        super(errors);
        this.statusCode = statusCode;
        this.statusMessage = statusMessage;
        this.responseBody = responseBody;
    }

    /**
     * @return HTTP response status code
     */
    public int getStatusCode() {
        return statusCode;
    }

    /**
     * @return HTTP response status message
     */
    public String getStatusMessage() {
        return statusMessage;
    }

    /**
     * @return HTTP response body (if provided)
     */
    @Nullable
    public String getResponseBody() {
        return responseBody;
    }

}
