package com.atlassian.stash.rest.client.api;

import com.atlassian.stash.rest.client.api.entity.ApplicationProperties;
import com.atlassian.stash.rest.client.api.entity.Branch;
import com.atlassian.stash.rest.client.api.entity.Comment;
import com.atlassian.stash.rest.client.api.entity.MirrorServer;
import com.atlassian.stash.rest.client.api.entity.Page;
import com.atlassian.stash.rest.client.api.entity.Project;
import com.atlassian.stash.rest.client.api.entity.ProjectGroupPermission;
import com.atlassian.stash.rest.client.api.entity.ProjectPermission;
import com.atlassian.stash.rest.client.api.entity.ProjectUserPermission;
import com.atlassian.stash.rest.client.api.entity.PullRequestMergeability;
import com.atlassian.stash.rest.client.api.entity.PullRequestParticipant;
import com.atlassian.stash.rest.client.api.entity.PullRequestRef;
import com.atlassian.stash.rest.client.api.entity.PullRequestStatus;
import com.atlassian.stash.rest.client.api.entity.Repository;
import com.atlassian.stash.rest.client.api.entity.Tag;
import com.atlassian.stash.rest.client.api.entity.Task;
import com.atlassian.stash.rest.client.api.entity.TaskAnchor;
import com.atlassian.stash.rest.client.api.entity.User;
import com.atlassian.stash.rest.client.api.entity.UserSshKey;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.Maps;
import org.hamcrest.Matcher;

import java.util.Collection;
import java.util.Date;
import java.util.Map;
import java.util.Optional;

public class EntityMatchers {

    public static UserMatcherBuilder user() {
        return new UserMatcherBuilder();
    }

    public static ProjectMatcherBuilder project() {
        return new ProjectMatcherBuilder();
    }

    public static ProjectGroupPermissionMatcherBuilder projectGroupPermission() {
        return new ProjectGroupPermissionMatcherBuilder();
    }

    public static ProjectUserPermissionMatcherBuilder projectUserPermission() {
        return new ProjectUserPermissionMatcherBuilder();
    }

    public static <T> PageMatcherBuilder<T> page(@SuppressWarnings("unused") Class<T> typeHint) {
        return page();
    }

    public static <T> PageMatcherBuilder<T> page() {
        return new PageMatcherBuilder<>();
    }

    public static RepositoryMatcherBuilder repository() {
        return new RepositoryMatcherBuilder();
    }

    public static BranchMatcherBuilder branch() {
        return new BranchMatcherBuilder();
    }

    public static TagMatcherBuilder tag(){
        return new TagMatcherBuilder();
    }

    public static UserSshKeyMatcherBuilder userSshKey() {
        return new UserSshKeyMatcherBuilder();
    }

    public static PullRequestParticipantMatcherBuilder pullRequestParticipant() {
        return new PullRequestParticipantMatcherBuilder();
    }

    public static PullRequestRefMatcherBuilder pullRequestRef() {
        return new PullRequestRefMatcherBuilder();
    }

    public static PullRequestStatusMatcherBuilder pullRequestStatus() {
        return new PullRequestStatusMatcherBuilder();
    }

    public static PullRequestMergeabilityMatcherBuilder pullRequestMergeability() {
        return new PullRequestMergeabilityMatcherBuilder();
    }

    public static CommentMatcherBuilder comment() {
        return new CommentMatcherBuilder();
    }

    public static TaskMatcherBuilder task() {
        return new TaskMatcherBuilder();
    }

    public static ApplicationPropertiesMatcherBuilder applicationProperties() {
        return new ApplicationPropertiesMatcherBuilder();
    }

    public static StashErrorMatcherBuilder stashError() {
        return new StashErrorMatcherBuilder();
    }

    public static MirrorServerMatcherBuilder mirrorServer() {
        return new MirrorServerMatcherBuilder();
    }

    static public class UserMatcherBuilder extends PropertyMatcherBuilder<User, UserMatcherBuilder> {

        UserMatcherBuilder() {
        }

        public UserMatcherBuilder name(Matcher<String> matcher) {
            return put("name", matcher);
        }

        public UserMatcherBuilder emailAddress(Matcher<String> matcher) {
            return put("emailAddress", matcher);
        }

        public UserMatcherBuilder id(Matcher<Long> matcher) {
            return put("id", matcher);
        }

        public UserMatcherBuilder displayName(Matcher<String> matcher) {
            return put("displayName", matcher);
        }

        public UserMatcherBuilder active(Matcher<Boolean> matcher) {
            return put("active", matcher);
        }

        public UserMatcherBuilder slug(Matcher<String> matcher) {
            return put("slug", matcher);
        }

        public UserMatcherBuilder type(Matcher<String> matcher) {
            return put("type", matcher);
        }

        public UserMatcherBuilder directoryName(Matcher<String> matcher) {
            return put("directoryName", matcher);
        }

        public UserMatcherBuilder deletable(Matcher<Boolean> matcher) {
            return put("deletable", matcher);
        }

        public UserMatcherBuilder lastAuthenticationTimestamp(Matcher<Date> matcher) {
            return put("lastAuthenticationTimestamp", matcher);
        }

        public UserMatcherBuilder mutableDetails(Matcher<Boolean> matcher) {
            return put("mutableDetails", matcher);
        }

        public UserMatcherBuilder mutableGroups(Matcher<Boolean> matcher) {
            return put("mutableGroups", matcher);
        }

        public UserMatcherBuilder selfUrl(Matcher<String> matcher) {
            return put("selfUrl", matcher);
       }

    }

    static public class ProjectMatcherBuilder extends PropertyMatcherBuilder<Project, ProjectMatcherBuilder> {

        ProjectMatcherBuilder() {
        }

        public ProjectMatcherBuilder key(Matcher<String> matcher) {
            return put("key", matcher);
        }

        public ProjectMatcherBuilder id(Matcher<Long> matcher) {
            return put("id", matcher);
        }

        public ProjectMatcherBuilder name(Matcher<String> matcher) {
            return put("name", matcher);
        }

        public ProjectMatcherBuilder description(Matcher<String> matcher) {
            return put("description", matcher);
        }

        public ProjectMatcherBuilder isPublic(Matcher<Boolean> matcher) {
            return put("public", matcher);
        }

        public ProjectMatcherBuilder isPersonal(Matcher<Boolean> matcher) {
            return put("personal", matcher);
        }

        public ProjectMatcherBuilder type(Matcher<String> matcher) {
            return put("type", matcher);
        }

        public ProjectMatcherBuilder selfUrl(Matcher<String> matcher) {
            return put("selfUrl", matcher);
        }
    }

    static public class ProjectGroupPermissionMatcherBuilder
            extends PropertyMatcherBuilder<ProjectGroupPermission, ProjectGroupPermissionMatcherBuilder> {
        ProjectGroupPermissionMatcherBuilder() {
        }

        public ProjectGroupPermissionMatcherBuilder groupName(Matcher<String> matcher) {
            return put("groupName", matcher);
        }

        public ProjectGroupPermissionMatcherBuilder permission(Matcher<ProjectPermission> matcher) {
            return put("permission", matcher);
        }
    }

    static public class ProjectUserPermissionMatcherBuilder
            extends PropertyMatcherBuilder<ProjectUserPermission, ProjectUserPermissionMatcherBuilder> {
        ProjectUserPermissionMatcherBuilder() {
        }

        public ProjectUserPermissionMatcherBuilder userName(Matcher<String> matcher) {
            return put("userName", matcher);
        }

        public ProjectUserPermissionMatcherBuilder permission(Matcher<ProjectPermission> matcher) {
            return put("permission", matcher);
        }
    }

    static public class PageMatcherBuilder<T> extends PropertyMatcherBuilder<Page<T>, PageMatcherBuilder<T>> {
        PageMatcherBuilder() {
        }

        public PageMatcherBuilder<T> size(Matcher<Integer> matcher) {
            return put("size", matcher);
        }

        public PageMatcherBuilder<T> limit(Matcher<Integer> matcher) {
            return put("limit", matcher);
        }

        public PageMatcherBuilder<T> values(Matcher<Collection<? extends T>> matcher) {
            return put("values", matcher);
        }

        public PageMatcherBuilder<T> valuesIterable(Matcher<Iterable<? extends T>> matcher) {
            return put("values", matcher);
        }
    }

    static public class RepositoryMatcherBuilder extends PropertyMatcherBuilder<Repository, RepositoryMatcherBuilder> {
        RepositoryMatcherBuilder() {
        }

        public RepositoryMatcherBuilder slug(Matcher<String> matcher) {
            return put("slug", matcher);
        }

        public RepositoryMatcherBuilder id(Matcher<Integer> matcher) {
            return put("id", matcher);
        }

        public RepositoryMatcherBuilder name(Matcher<String> matcher) {
            return put("name", matcher);
        }

        public RepositoryMatcherBuilder isPublic(Matcher<Boolean> matcher) {
            return put("public", matcher);
        }

        public RepositoryMatcherBuilder sshCloneUrl(Matcher<String> matcher) {
            return put("sshCloneUrl", matcher);
        }

        public RepositoryMatcherBuilder httpCloneUrl(Matcher<String> matcher) {
            return put("httpCloneUrl", matcher);
        }

        public RepositoryMatcherBuilder selfUrl(Matcher<String> matcher) {
            return put("selfUrl", matcher);
        }

        public RepositoryMatcherBuilder project(Matcher<Project> matcher) {
            return put("project", matcher);
        }

        public RepositoryMatcherBuilder origin(Matcher<Repository> matcher) {
            return put("origin", matcher);
        }
    }

    static public class BranchMatcherBuilder extends PropertyMatcherBuilder<Branch, BranchMatcherBuilder> {
        BranchMatcherBuilder() {
        }

        public BranchMatcherBuilder isDefault(Matcher<Boolean> matcher) {
            return put("default", matcher);
        }

        public BranchMatcherBuilder id(Matcher<String> matcher) {
            return put("id", matcher);
        }

        public BranchMatcherBuilder displayId(Matcher<String> matcher) {
            return put("displayId", matcher);
        }

        public BranchMatcherBuilder latestChangeset(Matcher<String> matcher) {
            return put("latestChangeset", matcher);
        }
    }

    static public class TagMatcherBuilder extends PropertyMatcherBuilder<Tag, TagMatcherBuilder>{

        public TagMatcherBuilder id(Matcher<String> matcher) {
            return put("id", matcher);
        }

        public TagMatcherBuilder displayId(Matcher<String> matcher) {
            return put("displayId", matcher);
        }

        public TagMatcherBuilder latestChangeset(Matcher<String> matcher) {
            return put("latestChangeset", matcher);
        }

        public TagMatcherBuilder latestCommit(Matcher<String> matcher) {
            return put("latestCommit", matcher);
        }

        public TagMatcherBuilder hash(Matcher<String> matcher) {
            return put("hash", matcher);
        }
    }

    static public class MirrorServerMatcherBuilder extends PropertyMatcherBuilder<MirrorServer, MirrorServerMatcherBuilder> {

        public MirrorServerMatcherBuilder id(Matcher<String> matcher) {
            return put("id", matcher);
        }

        public MirrorServerMatcherBuilder name(Matcher<String> matcher) {
            return put("name", matcher);
        }

        public MirrorServerMatcherBuilder selfUrl(Matcher<String> matcher) {
            return put("selfUrl", matcher);
        }

        public MirrorServerMatcherBuilder enabled(Matcher<Boolean> matcher) {
            return put("enabled", matcher);
        }

    }

    static public class UserSshKeyMatcherBuilder extends PropertyMatcherBuilder<UserSshKey, UserSshKeyMatcherBuilder> {
        UserSshKeyMatcherBuilder() {
        }

        public UserSshKeyMatcherBuilder id(Matcher<Long> matcher) {
            return put("id", matcher);
        }

        public UserSshKeyMatcherBuilder text(Matcher<String> matcher) {
            return put("text", matcher);
        }

        public UserSshKeyMatcherBuilder label(Matcher<String> matcher) {
            return put("label", matcher);
        }
    }

    static public class PullRequestParticipantMatcherBuilder extends PropertyMatcherBuilder<PullRequestParticipant, PullRequestParticipantMatcherBuilder> {
        PullRequestParticipantMatcherBuilder() {
        }

        public PullRequestParticipantMatcherBuilder name(Matcher<String> matcher) {
            return put("name", matcher);
        }

        public PullRequestParticipantMatcherBuilder displayName(Matcher<String> matcher) {
            return put("displayName", matcher);
        }

        public PullRequestParticipantMatcherBuilder slug(Matcher<String> matcher) {
            return put("slug", matcher);
        }

        public PullRequestParticipantMatcherBuilder avatarUrl(Matcher<String> matcher) {
            return put("avatarUrl", matcher);
        }

        public PullRequestParticipantMatcherBuilder approved(Matcher<Boolean> matcher) {
            return put("approved", matcher);
        }

        public PullRequestParticipantMatcherBuilder status(Matcher<String> matcher) {
            return put("status", matcher);
        }

        public PullRequestParticipantMatcherBuilder role(Matcher<String> matcher) {
            return put("role", matcher);
        }
    }

    static public class PullRequestRefMatcherBuilder extends PropertyMatcherBuilder<PullRequestRef, PullRequestRefMatcherBuilder> {
        PullRequestRefMatcherBuilder() {
        }

        public PullRequestRefMatcherBuilder repositorySlug(Matcher<String> matcher) {
            return put("repositorySlug", matcher);
        }

        public PullRequestRefMatcherBuilder repositoryName(Matcher<String> matcher) {
            return put("repositoryName", matcher);
        }

        public PullRequestRefMatcherBuilder projectKey(Matcher<String> matcher) {
            return put("projectKey", matcher);
        }

        public PullRequestRefMatcherBuilder projectName(Matcher<String> matcher) {
            return put("projectName", matcher);
        }

        public PullRequestRefMatcherBuilder id(Matcher<String> matcher) {
            return put("id", matcher);
        }

        public PullRequestRefMatcherBuilder displayId(Matcher<String> matcher) {
            return put("displayId", matcher);
        }
    }

    static public class PullRequestStatusMatcherBuilder extends PropertyMatcherBuilder<PullRequestStatus, PullRequestStatusMatcherBuilder> {
        PullRequestStatusMatcherBuilder() {
        }

        public PullRequestStatusMatcherBuilder id(Matcher<Long> matcher) {
            return put("id", matcher);
        }

        public PullRequestStatusMatcherBuilder version(Matcher<Long> matcher) {
            return put("version", matcher);
        }

        public PullRequestStatusMatcherBuilder title(Matcher<String> matcher) {
            return put("title", matcher);
        }

        public PullRequestStatusMatcherBuilder description(Matcher<Optional<String>> matcher) {
            return put("description", matcher);
        }

        public PullRequestStatusMatcherBuilder url(Matcher<String> matcher) {
            return put("url", matcher);
        }

        public PullRequestStatusMatcherBuilder mergeOutcome(Matcher<Optional<String>> matcher) {
            return put("mergeOutcome", matcher);
        }

        public PullRequestStatusMatcherBuilder author(Matcher<PullRequestParticipant> matcher) {
            return put("author", matcher);
        }

        public PullRequestStatusMatcherBuilder reviewers(Matcher<Iterable<? extends PullRequestParticipant>> matcher) {
            return put("reviewers", matcher);
        }

        public PullRequestStatusMatcherBuilder fromRef(Matcher<PullRequestRef> matcher) {
            return put("fromRef", matcher);
        }

        public PullRequestStatusMatcherBuilder toRef(Matcher<PullRequestRef> matcher) {
            return put("toRef", matcher);
        }

        public PullRequestStatusMatcherBuilder lastUpdated(Matcher<Long> matcher) {
            return put("lastUpdated", matcher);
        }

        public PullRequestStatusMatcherBuilder commentCount(Matcher<Optional<Long>> matcher) {
            return put("commentCount", matcher);
        }

        public PullRequestStatusMatcherBuilder outstandingTaskCount(Matcher<Optional<Long>> matcher) {
            return put("outstandingTaskCount", matcher);
        }

        public PullRequestStatusMatcherBuilder resolvedTaskCount(Matcher<Optional<Long>> matcher) {
            return put("resolvedTaskCount", matcher);
        }

        public PullRequestStatusMatcherBuilder state(Matcher<String> matcher) {
            return put("state", matcher);
        }
    }

    static public class PullRequestMergeabilityMatcherBuilder extends PropertyMatcherBuilder<PullRequestMergeability, PullRequestMergeabilityMatcherBuilder> {
        PullRequestMergeabilityMatcherBuilder() {
        }

        public PullRequestMergeabilityMatcherBuilder outcome(Matcher<Optional<String>> matcher) {
            return put("outcome", matcher);
        }

        public PullRequestMergeabilityMatcherBuilder canMerge(Matcher<Boolean> matcher) {
            return put("canMerge", matcher);
        }

        public PullRequestMergeabilityMatcherBuilder conflicted(Matcher<Boolean> matcher) {
            return put("conflicted", matcher);
        }
    }

    static public class CommentMatcherBuilder extends PropertyMatcherBuilder<Comment, CommentMatcherBuilder> {
        CommentMatcherBuilder() {
        }

        public CommentMatcherBuilder id(Matcher<Long> matcher) {
            return put("id", matcher);
        }

        public CommentMatcherBuilder version(Matcher<Long> matcher) {
            return put("version", matcher);
        }

        public CommentMatcherBuilder text(Matcher<String> matcher) {
            return put("text", matcher);
        }
    }

    static public class TaskMatcherBuilder extends PropertyMatcherBuilder<Task, TaskMatcherBuilder> {
        TaskMatcherBuilder() {
        }

        public TaskMatcherBuilder id(Matcher<Long> matcher) {
            return put("id", matcher);
        }

        public TaskMatcherBuilder text(Matcher<String> matcher) {
            return put("text", matcher);
        }

        public TaskMatcherBuilder state(Matcher<String> matcher) {
            return put("state", matcher);
        }

        public TaskMatcherBuilder anchor(Matcher<TaskAnchor> matcher) {
            return put("anchor", matcher);
        }

        public TaskMatcherBuilder commentAnchor(Matcher<Comment> matcher) {
            return put("anchor", matcher);
        }
    }

    static public class ApplicationPropertiesMatcherBuilder extends PropertyMatcherBuilder<ApplicationProperties, ApplicationPropertiesMatcherBuilder> {
        ApplicationPropertiesMatcherBuilder() {
        }

        public ApplicationPropertiesMatcherBuilder version(Matcher<String> matcher) {
            return put("version", matcher);
        }

        public ApplicationPropertiesMatcherBuilder buildNumber(Matcher<String> matcher) {
            return put("buildNumber", matcher);
        }

        public ApplicationPropertiesMatcherBuilder buildDate(Matcher<String> matcher) {
            return put("buildDate", matcher);
        }

        public ApplicationPropertiesMatcherBuilder displayName(Matcher<String> matcher) {
            return put("displayName", matcher);
        }
    }

    static public class StashErrorMatcherBuilder extends PropertyMatcherBuilder<StashError, StashErrorMatcherBuilder> {
        StashErrorMatcherBuilder() {
        }

        public StashErrorMatcherBuilder message(Matcher<String> matcher) {
            return put("message", matcher);
        }

        public StashErrorMatcherBuilder context(Matcher<String> matcher) {
            return put("context", matcher);
        }

        public StashErrorMatcherBuilder exceptionName(Matcher<String> matcher) {
            return put("exceptionName", matcher);
        }
    }

    static class PropertyMatcherBuilder<T, B extends PropertyMatcherBuilder> {
        private Map<String, Matcher<?>> propertyNameAndMatchers = Maps.newLinkedHashMap();

        PropertyMatcherBuilder() {
        }

        @SuppressWarnings("unchecked")
        public B put(String property, Matcher<?> matcher) {
            propertyNameAndMatchers.put(property, matcher);
            return (B) this;
        }

        public Matcher<T> build() {
            return new PropertyMatcher<>(ImmutableMap.copyOf(propertyNameAndMatchers));
        }
    }
}
