package it.com.atlassian.bbmirror.rest.client.tests;

import com.atlassian.applinks.api.ApplicationLink;
import com.atlassian.applinks.api.ApplicationLinkService;
import com.atlassian.applinks.spi.link.MutatingApplicationLinkService;
import com.atlassian.applinks.spi.manifest.ManifestRetriever;
import com.atlassian.applinks.spi.util.TypeAccessor;
import com.atlassian.bamboo.applinks.ImpersonationService;
import com.atlassian.plugins.osgi.test.AtlassianPluginsTestRunner;
import com.atlassian.stash.rest.client.api.StashClient;
import com.atlassian.stash.rest.client.applinks.ApplinkStashClientFactoryImpl;
import it.com.atlassian.stash.rest.client.tests.ApplinkOnDemand;
import it.com.atlassian.stash.rest.client.tests.ImpersonatingStashClient;
import it.com.atlassian.stash.rest.client.tests.TestUtil;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.runner.RunWith;

import java.util.concurrent.Callable;

import static it.com.atlassian.stash.rest.client.tests.TestUtil.HOST_ADMIN_LOGIN;
import static it.com.atlassian.stash.rest.client.tests.TestUtil.STASH_ADMIN_LOGIN;
import static it.com.atlassian.stash.rest.client.tests.TestUtil.STASH_ADMIN_PASSWORD;

@RunWith(AtlassianPluginsTestRunner.class)
public class ApplinkMirrorStashClientWiredTest extends MirrorStashClientIntegrationTestBase {
    private final ApplicationLinkService linkService;
    private final ImpersonationService impersonationService;
    // class set up
    private final ApplinkOnDemand applinkOD;

    public ApplinkMirrorStashClientWiredTest(MutatingApplicationLinkService linkService, TypeAccessor typeAccessor,
                                             ManifestRetriever manifestRetriever, ImpersonationService impersonationService) {
        this.linkService = linkService;
        this.impersonationService = impersonationService;

        this.applinkOD = new ApplinkOnDemand(linkService, typeAccessor, manifestRetriever);
    }

    @BeforeClass
    public void setUpClass() throws Exception {
        impersonationService.runAsUser(HOST_ADMIN_LOGIN, (Callable<Void>) () -> {
            applinkOD.createTrustedAppsLink(TestUtil.getProductUrl("stash.or.bitbucket"), "stash/BBS",
                    STASH_ADMIN_LOGIN, STASH_ADMIN_PASSWORD);
            return null;
        }).call();
    }

    @AfterClass
    public void tearDownClass() throws Exception {
        impersonationService.runAsUser(HOST_ADMIN_LOGIN, (Callable<Void>) () -> {
            applinkOD.deleteCreated();
            return null;
        }).call();
    }

    @Override
    protected StashClient createStashClient(final String stashUsername, final String stashPassword) throws Exception {
        try {
            return impersonationService.runAsUser(stashUsername, () -> {
                ApplinkStashClientFactoryImpl factory = new ApplinkStashClientFactoryImpl(linkService);
                ApplicationLink stashApplink = applinkOD.get();
                return new ImpersonatingStashClient(factory.getStashClient(stashApplink), impersonationService, stashUsername);
            }).call();
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

}
