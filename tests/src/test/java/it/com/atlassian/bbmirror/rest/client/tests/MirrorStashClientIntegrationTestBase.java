package it.com.atlassian.bbmirror.rest.client.tests;

import com.atlassian.stash.rest.client.api.StashRestException;
import com.atlassian.stash.rest.client.api.StashVersions;
import com.atlassian.stash.rest.client.api.entity.MirrorServer;
import com.atlassian.stash.rest.client.api.entity.Page;
import com.atlassian.stash.rest.client.api.entity.Repository;
import it.com.atlassian.stash.rest.client.tests.StashClientIntegrationTestCommon;
import org.junit.Test;
import org.junit.matchers.JUnitMatchers;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.notNullValue;
import static org.junit.Assert.assertThat;

public abstract class MirrorStashClientIntegrationTestBase extends StashClientIntegrationTestCommon {

    @Test
    public void testGetRepositoryMirrors() {
        //given
        assumeStashVersionAtLeast(StashVersions.SMART_MIRRORING);
        Repository repository = service.getRepository("PWM", "mirrored-repo");

        //when
        StashRestException stashRestException = null;
        final Page<MirrorServer> mirrors = service.getRepositoryMirrors(repository.getId(), 0, 25);

        //then
        assertThat(mirrors.getSize(), is(1));
        final MirrorServer mirrorServer = mirrors.getValues().get(0);
        assertThat(mirrorServer.getSelfUrl(), JUnitMatchers.containsString("7991"));
        assertThat(mirrorServer.isEnabled(), is(true));
        assertThat(mirrorServer.getName(), notNullValue());
        assertThat(mirrorServer.getId(), notNullValue());
    }

}
