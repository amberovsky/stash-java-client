package it.com.atlassian.stash.rest.client.tests;

import com.atlassian.bamboo.applinks.ImpersonationService;
import com.atlassian.stash.rest.client.api.AvatarRequest;
import com.atlassian.stash.rest.client.api.StashClient;
import com.atlassian.stash.rest.client.api.entity.ApplicationProperties;
import com.atlassian.stash.rest.client.api.entity.Branch;
import com.atlassian.stash.rest.client.api.entity.Comment;
import com.atlassian.stash.rest.client.api.entity.MirrorServer;
import com.atlassian.stash.rest.client.api.entity.Page;
import com.atlassian.stash.rest.client.api.entity.Permission;
import com.atlassian.stash.rest.client.api.entity.Project;
import com.atlassian.stash.rest.client.api.entity.ProjectGroupPermission;
import com.atlassian.stash.rest.client.api.entity.ProjectPermission;
import com.atlassian.stash.rest.client.api.entity.ProjectUserPermission;
import com.atlassian.stash.rest.client.api.entity.PullRequestMergeability;
import com.atlassian.stash.rest.client.api.entity.PullRequestRef;
import com.atlassian.stash.rest.client.api.entity.PullRequestStatus;
import com.atlassian.stash.rest.client.api.entity.Repository;
import com.atlassian.stash.rest.client.api.entity.RepositorySshKey;
import com.atlassian.stash.rest.client.api.entity.Tag;
import com.atlassian.stash.rest.client.api.entity.Task;
import com.atlassian.stash.rest.client.api.entity.TaskAnchor;
import com.atlassian.stash.rest.client.api.entity.TaskState;
import com.atlassian.stash.rest.client.api.entity.User;
import com.atlassian.stash.rest.client.api.entity.UserSshKey;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.util.Map;
import java.util.Optional;
import java.util.concurrent.Callable;

public class ImpersonatingStashClient implements StashClient {
    private final StashClient stashService;
    private final ImpersonationService impersonationService;
    private final String username;

    public ImpersonatingStashClient(StashClient stashService, ImpersonationService impersonationService, String username) {
        this.stashService = stashService;
        this.impersonationService = impersonationService;
        this.username = username;
    }

    @Override
    public boolean isUserKey(final @Nonnull String publicKey) {
        return doImpersonate(() -> stashService.isUserKey(publicKey));
    }

    @Nonnull
    @Override
    public Page<User> getUsers(@Nullable final String filterForUsername, final long start, final long limit) {
        return doImpersonate(() -> stashService.getUsers(filterForUsername, start, limit));
    }

    @Nonnull
    @Override
    public Page<Repository> getRepositories(
            @Nullable final String projectName,
            @Nullable final String query, final long start, final long limit) {
        return doImpersonate(() -> stashService.getRepositories(projectName, query, start, limit));
    }

    @Nonnull
    @Override
    public Page<Repository> getProjectRepositories(@Nonnull final String projectKey, final long start, final long limit) {
        return doImpersonate(() -> stashService.getProjectRepositories(projectKey, start, limit));
    }

    @Override
    public Repository getRepository(@Nonnull final String projectKey,
                                    @Nonnull final String repositorySlug) {
        return doImpersonate(() -> stashService.getRepository(projectKey, repositorySlug));
    }

    @Override
    public boolean isRepositoryKey(final @Nonnull String projectKey,
                                   final @Nonnull String repositorySlug,
                                   final @Nonnull String publicKey) {
        return doImpersonate(() -> stashService.isRepositoryKey(projectKey, repositorySlug, publicKey));
    }

    @Nonnull
    @Override
    public Map<String, String> getStashApplicationProperties() {
        return doImpersonate(stashService::getStashApplicationProperties);
    }

    @Nonnull
    @Override
    public Page<RepositorySshKey> getRepositoryKeys(@Nonnull final String projectKey,
                                                    @Nonnull final String repositorySlug,
                                                    final long start,
                                                    final long limit) {
        return doImpersonate(() -> stashService.getRepositoryKeys(projectKey, repositorySlug, start, limit));
    }

    @Override
    public boolean addUserKey(@Nonnull final String publicKey, @Nullable final String keyLabel) {
        return doImpersonate(() -> stashService.addUserKey(publicKey, keyLabel));
    }

    @Override
    public boolean removeUserKey(@Nonnull final String publicKey) {
        return doImpersonate(() -> stashService.removeUserKey(publicKey));
    }

    @Override
    public boolean removeUserKey(final long keyId) {

        return doImpersonate(() -> stashService.removeUserKey(keyId));
    }

    @Override
    public boolean projectExists(@Nonnull String projectKey)
    {
        return doImpersonate(() -> stashService.projectExists(projectKey));
    }

    @Override
    public boolean createProject(final @Nonnull String projectKey, @Nonnull final String name,
                                 @Nonnull final String type, @Nonnull final String description) {
        return doImpersonate(() -> stashService.createProject(projectKey, name, type, description));
    }

    @Override
    public boolean updateProject(@Nonnull String projectKey, @Nonnull String newProjectKey,
                                 @Nonnull String name, @Nullable String description) {
        return doImpersonate(() -> stashService.updateProject(projectKey, newProjectKey, name, description));
    }

    @Override
    public Page<ProjectGroupPermission> getProjectGroupPermissions(@Nonnull final String projectKey, @Nullable final String filter,
                                                                   final long start, final long limit) {
        return doImpersonate(() -> stashService.getProjectGroupPermissions(projectKey, filter, start, limit));
    }

    @Override
    public boolean addProjectGroupPermission(@Nonnull String projectKey, @Nonnull String groupName,
                                             @Nonnull ProjectPermission permission) {
        return doImpersonate(() -> stashService.addProjectGroupPermission(projectKey, groupName, permission));
    }

    @Override
    public Page<ProjectUserPermission> getProjectUserPermissions(@Nonnull final String projectKey, @Nullable final String filter,
                                                                 final long start, final long limit) {
        return doImpersonate(() -> stashService.getProjectUserPermissions(projectKey, filter, start, limit));
    }

    @Override
    public boolean addProjectUserPermission(@Nonnull String projectKey, @Nonnull String userName,
                                            @Nonnull ProjectPermission permission) {
        return doImpersonate(() -> stashService.addProjectUserPermission(projectKey, userName, permission));
    }

    @Override
    public boolean createRepository(@Nonnull final String projectKey, @Nonnull final String name,
                                    @Nonnull final String scmId, final boolean forkable) {
        return doImpersonate(() -> stashService.createRepository(projectKey, name, scmId, forkable));
    }

    @Override
    public boolean deleteProject(@Nonnull final String projectKey) {
        return doImpersonate(() -> stashService.deleteProject(projectKey));
    }

    @Override
    public boolean deleteRepository(@Nonnull final String projectKey, @Nonnull final String repositorySlug) {
        return doImpersonate(() -> stashService.deleteRepository(projectKey, repositorySlug));
    }

    @Nonnull
    @Override
    public Page<Project> getAccessibleProjects(final long start,
                                               final long limit) {
        return doImpersonate(() -> stashService.getAccessibleProjects(start, limit));
    }

    @Nonnull
    @Override
    public Page<Branch> getRepositoryBranches(@Nonnull final String projectKey,
                                              @Nonnull final String repositorySlug,
                                              @Nullable final String query,
                                              final long start,
                                              final long limit) {
        return doImpersonate(() -> stashService.getRepositoryBranches(projectKey, repositorySlug, query, start, limit));
    }

    @Override
    public Page<Tag> getRepositoryTags(@Nonnull final String projectKey,
                                       @Nonnull final String repositorySlug,
                                       @Nullable final String query,
                                       final long start,
                                       final long limit) {
        return doImpersonate(() -> stashService.getRepositoryTags(projectKey, repositorySlug, query, start, limit));
    }

    @Nonnull
    @Override
    public Page<MirrorServer> getRepositoryMirrors(final long repositoryId, final long start, final long limit) {
        return doImpersonate(() -> stashService.getRepositoryMirrors(repositoryId, start, limit));
    }

    @Nullable
    @Override
    public Branch getRepositoryDefaultBranch(@Nonnull final String projectKey, @Nonnull final String repositorySlug) {
        return doImpersonate(() -> stashService.getRepositoryDefaultBranch(projectKey, repositorySlug));
    }

    @Override
    public boolean addRepositoryKey(@Nonnull final String projectKey,
                                    @Nonnull final String repositorySlug,
                                    @Nonnull final String publicKey,
                                    @Nullable final String keyLabel,
                                    @Nonnull final Permission keyPermission) {
        return doImpersonate(() -> stashService.addRepositoryKey(projectKey, repositorySlug, publicKey, keyLabel,
                keyPermission));
    }

    @Nonnull
    @Override
    public Page<UserSshKey> getCurrentUserKeys(final long start, final long limit) {
        return doImpersonate(() -> stashService.getCurrentUserKeys(start, limit));
    }

    private <T> T doImpersonate(Callable<T> callable) {
        try {
            return impersonationService.runAsUser(username, callable).call();
        } catch (RuntimeException e) {
            throw e;
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public Optional<Permission> getCurrentUserRepositoryPermission(@Nonnull final String projectKey,
                                                                   @Nonnull final String repositorySlug) {
        return doImpersonate(() -> stashService.getCurrentUserRepositoryPermission(projectKey, repositorySlug));
    }

    @Override
    public boolean addRepositoryUserPermission(@Nonnull final String projectKey, @Nonnull final String repositorySlug,
                                               @Nonnull final String userName, @Nonnull final Permission permission) {
        return doImpersonate(() -> stashService.addRepositoryUserPermission(projectKey, repositorySlug, userName,
                permission));
    }

    @Nonnull
    @Override
    public PullRequestStatus createPullRequest(@Nonnull final String title, @Nullable final String description,
                                               @Nonnull final PullRequestRef fromRef,
                                               @Nonnull final PullRequestRef toRef,
                                               @Nonnull final Iterable<String> reviewers,
                                               @Nullable AvatarRequest avatarRequest) {
        return doImpersonate(() -> stashService.createPullRequest(title, description, fromRef, toRef, reviewers,
                avatarRequest));
    }

    @Nonnull
    @Override
    public Page<PullRequestStatus> getPullRequestsByRepository(@Nonnull final String projectKey,
                                                               @Nonnull final String repositorySlug,
                                                               @Nullable final String branchName,
                                                               @Nullable final PullRequestDirection direction,
                                                               @Nullable final PullRequestStateFilter stateFilter,
                                                               @Nullable final PullRequestsOrder order,
                                                               final long start, final long limit,
                                                               @Nullable AvatarRequest avatarRequest) {
        return doImpersonate(() -> stashService.getPullRequestsByRepository(projectKey, repositorySlug, branchName,
                direction, stateFilter, order, start, limit, avatarRequest));
    }

    @Nonnull
    @Override
    public PullRequestStatus mergePullRequest(@Nonnull final String projectKey, @Nonnull final String repositorySlug,
                                              final long pullRequestId, final long version,
                                              @Nullable AvatarRequest avatarRequest) {
        return doImpersonate(() -> stashService.mergePullRequest(projectKey, repositorySlug, pullRequestId, version,
                avatarRequest));
    }

    @Nonnull
    @Override
    public PullRequestMergeability canMergePullRequest(@Nonnull final String projectKey,
                                                       @Nonnull final String repositorySlug,
                                                       final long pullRequestId) {
        return doImpersonate(() -> stashService.canMergePullRequest(projectKey, repositorySlug, pullRequestId));
    }

    @Nonnull
    @Override
    public Repository forkRepository(@Nonnull final String sourceProjectKey, @Nonnull final String sourceRepositorySlug,
                                     @Nonnull final String targetProjectKey, @Nonnull final String targetRepositorySlug) {
        return doImpersonate(() -> stashService.forkRepository(sourceProjectKey, sourceRepositorySlug, targetProjectKey,
                targetRepositorySlug));
    }

    @Nonnull
    @Override
    public Comment addPullRequestGeneralComment(@Nonnull final String projectKey, @Nonnull final String repositorySlug,
                                                long pullRequestId, @Nonnull final String text) {
        return doImpersonate(() -> stashService.addPullRequestGeneralComment(projectKey, repositorySlug, pullRequestId,
                text));
    }

    @Nonnull
    @Override
    public Task addTask(@Nonnull final TaskAnchor anchor, @Nonnull final String text) {
        return doImpersonate(() -> stashService.addTask(anchor, text));
    }

    @Nonnull
    @Override
    public Task updateTask(final long taskId, final TaskState taskState, final String text) {
        return doImpersonate(() -> stashService.updateTask(taskId, taskState, text));
    }

    @Nonnull
    @Override
    public ApplicationProperties getApplicationProperties() {
        return doImpersonate(stashService::getApplicationProperties);
    }
}
