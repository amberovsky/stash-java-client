package it.com.atlassian.stash.rest.client.tests;

import com.atlassian.applinks.api.ApplicationLink;
import com.atlassian.applinks.api.application.stash.StashApplicationType;
import com.atlassian.applinks.spi.link.MutatingApplicationLinkService;
import com.atlassian.applinks.spi.manifest.ManifestRetriever;
import com.atlassian.applinks.spi.util.TypeAccessor;
import com.atlassian.plugins.osgi.test.AtlassianPluginsTestRunner;
import com.google.common.collect.Iterables;
import org.junit.Test;
import org.junit.runner.RunWith;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

@RunWith(AtlassianPluginsTestRunner.class)
public class DeleteStashApplinkWiredTest {
    private final ApplinkOnDemand applinkOD;

    public DeleteStashApplinkWiredTest(MutatingApplicationLinkService linkService, TypeAccessor typeAccessor,
                                    ManifestRetriever manifestRetriever) {
        this.applinkOD = new ApplinkOnDemand(linkService, typeAccessor, manifestRetriever);
    }

    @Test
    public void testDeleteStashApplink() throws Exception {
        // when
        applinkOD.deleteByType(StashApplicationType.class);

        // then
        Iterable<ApplicationLink> stashApplinks = applinkOD.listByType(StashApplicationType.class);
        assertThat("no Stash applinks", Iterables.size(stashApplinks), is(0));
    }
}
