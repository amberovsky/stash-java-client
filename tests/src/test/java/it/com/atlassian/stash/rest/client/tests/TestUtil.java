package it.com.atlassian.stash.rest.client.tests;

import org.bouncycastle.jce.provider.JDKKeyPairGenerator;
import org.bouncycastle.openssl.PEMWriter;
import org.bouncycastle.util.encoders.Base64;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.StringWriter;
import java.security.Key;
import java.security.KeyPair;
import java.security.interfaces.RSAPublicKey;

public class TestUtil {
    private static final Logger log = LoggerFactory.getLogger(TestUtil.class);
    public static final String HOST_ADMIN_LOGIN = "admin";
    public static final String STASH_ADMIN_LOGIN = "admin";
    public static final String STASH_ADMIN_PASSWORD = "admin";
    public static final String STASH_USER_LOGIN = "user";
    public static final String STASH_USER_PASSWORD = "user";

    public static String getProductUrl(String productId) {
        final String productUrl = System.getProperty("baseurl." + productId);
        log.info("Using product URL: <{}> for product: <{}>", productUrl, productId);
        return productUrl;
    }

    public static SshKeyPair generateSshKey(int strength, String label) {
        try {
            JDKKeyPairGenerator.RSA generator = new JDKKeyPairGenerator.RSA();
            generator.initialize(strength);
            KeyPair keyPair = generator.generateKeyPair();

            String privateKey = toPemFormat(keyPair.getPrivate());
            String publicKey = toPkcsFormat((RSAPublicKey) keyPair.getPublic(), label);

            return new SshKeyPair(privateKey, publicKey);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    public static class SshKeyPair {
        private final String privateKey;
        private final String publicKey;

        public SshKeyPair(String privateKey, String publicKey) {
            this.privateKey = privateKey;
            this.publicKey = publicKey;
        }

        public String getPrivateKey() {
            return privateKey;
        }

        public String getPublicKey() {
            return publicKey;
        }
    }

    private static String toPemFormat(Key privateKey) throws IOException {
        StringWriter w = new StringWriter();
        PEMWriter pemWriter = new PEMWriter(w);
        pemWriter.writeObject(privateKey);
        pemWriter.close();
        return w.toString();
    }

    private static String toPkcsFormat(RSAPublicKey rsaPublicKey, final String userComment) throws IOException
    {
        ByteArrayOutputStream output = new ByteArrayOutputStream();
        DataOutputStream dos = new DataOutputStream(output);
        dos.writeInt("ssh-rsa".getBytes().length);
        dos.write("ssh-rsa".getBytes());
        dos.writeInt(rsaPublicKey.getPublicExponent().toByteArray().length);
        dos.write(rsaPublicKey.getPublicExponent().toByteArray());
        dos.writeInt(rsaPublicKey.getModulus().toByteArray().length);
        dos.write(rsaPublicKey.getModulus().toByteArray());
        String enc = new String(Base64.encode(output.toByteArray()));
        dos.write(enc.getBytes());

        return "ssh-rsa " + enc + " " + userComment;
    }
}
