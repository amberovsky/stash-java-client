<?xml version="1.0" encoding="UTF-8"?>
<project xmlns="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/maven-v4_0_0.xsd">

    <modelVersion>4.0.0</modelVersion>

    <parent>
        <artifactId>stash-java-client</artifactId>
        <groupId>com.atlassian.stash</groupId>
        <version>2.3.1-SNAPSHOT</version>
    </parent>

    <artifactId>stash-java-client-tests</artifactId>

    <name>stash-java-client-tests</name>
    <description>Stash REST client tests</description>
    <packaging>atlassian-plugin</packaging>

    <dependencies>

        <dependency>
            <groupId>com.atlassian.stash</groupId>
            <artifactId>stash-java-client-applinks</artifactId>
            <version>${project.version}</version>
            <scope>compile</scope>
        </dependency>

        <dependency>
            <groupId>com.atlassian.stash</groupId>
            <artifactId>stash-java-client-httpclient</artifactId>
            <version>${project.version}</version>
            <scope>compile</scope>
        </dependency>

        <dependency>
            <groupId>com.atlassian.applinks</groupId>
            <artifactId>applinks-api</artifactId>
            <scope>provided</scope>
        </dependency>

        <dependency>
            <groupId>com.atlassian.applinks</groupId>
            <artifactId>applinks-plugin</artifactId>
            <scope>runtime</scope>
        </dependency>

        <dependency>
            <groupId>com.atlassian.applinks</groupId>
            <artifactId>applinks-spi</artifactId>
            <scope>provided</scope>
        </dependency>

        <dependency>
            <groupId>com.google.guava</groupId>
            <artifactId>guava</artifactId>
            <scope>provided</scope>
        </dependency>



        <dependency>
            <groupId>junit</groupId>
            <artifactId>junit</artifactId>
            <scope>provided</scope>
        </dependency>

        <dependency>
            <groupId>com.atlassian.stash</groupId>
            <artifactId>stash-java-client-api</artifactId>
            <version>${project.version}</version>
            <type>test-jar</type>
            <scope>test</scope>
        </dependency>

        <dependency>
            <groupId>javax.servlet</groupId>
            <artifactId>servlet-api</artifactId>
            <version>2.4</version>
            <scope>provided</scope>
        </dependency>

        <dependency>
            <groupId>com.atlassian.sal</groupId>
            <artifactId>sal-api</artifactId>
            <scope>provided</scope>
        </dependency>

        <dependency>
            <groupId>org.slf4j</groupId>
            <artifactId>slf4j-simple</artifactId>
            <version>1.5.8</version>
        </dependency>

        <dependency>
            <groupId>com.atlassian.bamboo</groupId>
            <artifactId>atlassian-bamboo-api</artifactId>
            <version>${bamboo.version}</version>
            <scope>provided</scope>
        </dependency>

        <dependency>
            <groupId>org.bouncycastle</groupId>
            <artifactId>bcprov-jdk16</artifactId>
            <version>1.46</version>
            <scope>test</scope>
        </dependency>

        <!-- WIRED TEST RUNNER DEPENDENCIES -->
        <dependency>
            <groupId>com.atlassian.plugins</groupId>
            <artifactId>atlassian-plugins-osgi-testrunner</artifactId>
            <version>${plugin.testrunner.version}</version>
            <scope>provided</scope>
        </dependency>
        <dependency>
            <groupId>com.atlassian.plugins</groupId>
            <artifactId>atlassian-plugins-osgi-testrunner-bundle</artifactId>
            <version>${plugin.testrunner.version}</version>
            <scope>runtime</scope>
        </dependency>
        <dependency>
            <groupId>javax.ws.rs</groupId>
            <artifactId>jsr311-api</artifactId>
            <version>1.1.1</version>
            <scope>provided</scope>
        </dependency>
        <dependency>
            <groupId>com.google.code.gson</groupId>
            <artifactId>gson</artifactId>
        </dependency>
    </dependencies>

    <build>
        <plugins>
            <plugin>
                <groupId>com.atlassian.maven.plugins</groupId>
                <artifactId>maven-amps-plugin</artifactId>
                <version>${amps.version}</version>
                <extensions>true</extensions>
                <configuration>
                    <systemPropertyVariables>
                        <baseurl.stash.or.bitbucket>http://localhost:${stash.or.bitbucket.port}/${stash.or.bitbucket.product}</baseurl.stash.or.bitbucket>
                    </systemPropertyVariables>
                    <extractDependencies>false</extractDependencies>
                    <products>
                        <product>
                            <id>bamboo</id>
                            <instanceId>bamboo</instanceId>
                            <version>${bamboo.version}</version>
                            <productDataPath>${basedir}/src/test/conf/bamboo-test-resources.zip</productDataPath>
                            <httpPort>${bamboo.port}</httpPort>
                            <jvmArgs>${bamboo.jvmargs}</jvmArgs>
                            <output>${project.build.directory}/bamboo.log</output>

                            <bundledArtifacts>
                                <!-- atlassian-plugins-osgi-testrunner requires commons-io < 2.0 -->
                                <bundledArtifact>
                                    <groupId>org.apache.servicemix.bundles</groupId>
                                    <artifactId>org.apache.servicemix.bundles.commons-io</artifactId>
                                    <version>1.4_3</version>
                                </bundledArtifact>
                            </bundledArtifacts>
                        </product>
                        <product>
                            <id>${stash.or.bitbucket.product}</id>
                            <instanceId>${stash.or.bitbucket.product}</instanceId>
                            <version>${stash.or.bitbucket.version}</version>
                            <httpPort>${stash.or.bitbucket.port}</httpPort>
                            <output>${project.build.directory}/${stash.or.bitbucket.product}.log</output>
                            <installPlugin>false</installPlugin>
                            <productDataPath>${basedir}/src/test/conf/${stash.or.bitbucket.product}-test-resources.zip</productDataPath>
                            <jvmArgs>${stash.or.bitbucket.jvmargs}</jvmArgs>
                        </product>
                        <product>
                            <id>bitbucket</id>
                            <instanceId>bitbucket-mirror</instanceId>
                            <version>4.14.4</version>
                            <contextPath>${bitbucket.mirror.contextPath}</contextPath>
                            <productDataPath>${project.basedir}/src/test/conf/bitbucket-mirror-home.zip
                            </productDataPath>
                            <httpPort>${bitbucket.mirror.httpPort}</httpPort>
                            <ajpPort>${bitbucket.mirror.ajpPort}</ajpPort>
                            <systemPropertyVariables>
                                <file.encoding>UTF-8</file.encoding>
                                <!-- pin the mirror's SSH server to port 7998 so it doesn't interfere with the upstream -->
                                <plugin.ssh.port>${bitbucket.mirror.ssh.port}</plugin.ssh.port>
                                <stash.test.smtp.port>9998</stash.test.smtp.port>
                            </systemPropertyVariables>
                        </product>
                        <product>
                            <id>bitbucket</id>
                            <instanceId>bitbucket-upstream</instanceId>
                            <version>4.14.4</version>
                            <contextPath>${bitbucket.upstream.contextPath}</contextPath>
                            <productDataPath>${project.basedir}/src/test/conf/bitbucket-upstream-home.zip
                            </productDataPath>
                            <httpPort>${bitbucket.upstream.httpPort}</httpPort>
                            <ajpPort>${bitbucket.upstream.ajpPort}</ajpPort>
                            <systemPropertyVariables>
                                <file.encoding>UTF-8</file.encoding>
                                <stash.test.smtp.port>9990</stash.test.smtp.port>
                                <plugin.connect.http.request.timeout>20</plugin.connect.http.request.timeout>
                                <plugin.connect.http.connection.timeout>10</plugin.connect.http.connection.timeout>
                                <plugin.connect.http.socket.timeout>10</plugin.connect.http.socket.timeout>
                            </systemPropertyVariables>
                        </product>
                    </products>

                    <testGroups>
                        <testGroup>
                            <id>stash-integration-tests</id>
                            <productIds>
                                <productId>bamboo</productId>
                                <productId>${stash.or.bitbucket.product}</productId>
                            </productIds>
                            <includes>
                                <include>it/com/atlassian/stash/**/*Test.java</include>
                            </includes>
                            <excludes>
                                <exclude>it/**/DeleteStashApplinkWiredTest.java</exclude>
                            </excludes>
                        </testGroup>
                        <testGroup>
                            <id>bitbucket-with-mirror-integration-tests</id>
                            <productIds>
                                <productId>bamboo</productId>
                                <productId>bitbucket-upstream</productId>
                                <!-- as of today mirror instance doesn't have to be running.
                                     it is enough that it was once registered in the upstream -->
                                <!--<productId>bitbucket-mirror</productId>-->
                            </productIds>
                            <includes>
                                <include>it/com/atlassian/bbmirror/**/*Test.java</include>
                            </includes>
                        </testGroup>
                    </testGroups>
                </configuration>
            </plugin>

        </plugins>
    </build>

</project>
