package com.atlassian.stash.rest.client.applinks;

import com.atlassian.applinks.api.ApplicationLink;
import com.atlassian.stash.rest.client.api.StashClient;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

/**
 * Service for calling out to Stash Server
 */
public interface ApplinkStashClientFactory {
    /**
     * @return a list of all configured Stash application links
     */
    @Nonnull
    Iterable<ApplicationLink> getStashApplicationLinks();

    /**
     * Retrieve a specific application link, does not check if its a stash link...
     *
     * @param serverKey uuid of application link
     * @return application link matching the uuid
     */
    @Nullable
    ApplicationLink getApplicationLink(@Nonnull String serverKey);

    /**
     * Get Stash REST client
     *
     * @param applicationLink Stash server to talk to
     * @return Stash REST client ready to talk to Stash server
     */
    @Nonnull
    StashClient getStashClient(@Nonnull final ApplicationLink applicationLink);
}
